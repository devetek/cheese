<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Ticket_answer",
 *      required={""},
 *      @SWG\Property(
 *          property="id_ticket_answer",
 *          description="id_ticket_answer",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_ticket",
 *          description="id_ticket",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="answer",
 *          description="answer",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="has_approved",
 *          description="has_approved",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Ticket_answer extends Model
{

    public $table = 'ticket_answer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_ticket_answer';

    public $fillable = [
        'id_ticket',
        'date_answer',
        'id_user',
        'answer',
        'has_approved',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_ticket_answer' => 'integer',
        'id_ticket' => 'integer',
        'id_user' => 'integer',
        'answer' => 'string',
        'has_approved' => 'string',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
