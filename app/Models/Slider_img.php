<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Slider_img",
 *      required={""},
 *      @SWG\Property(
 *          property="id_gallery_img",
 *          description="id_gallery_img",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_gallery",
 *          description="id_gallery",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="img",
 *          description="img",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum",
 *          description="thum",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      )
 * )
 */
class Slider_img extends Model
{

    public $table = 'gallery_img';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_gallery_img';

    public $fillable = [
        'id_gallery',
        'img',
        'thum',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_gallery_img' => 'integer',
        'id_gallery' => 'integer',
        'img' => 'string',
        'thum' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
