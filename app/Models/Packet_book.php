<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_book",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_book",
 *          description="id_packet_book",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_book",
 *          description="id_book",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Packet_book extends Model
{

    public $table = 'packet_book';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_book';

    public $fillable = [
        'id_book',
        'id_packet',
        'validasi',
        'id_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_book' => 'integer',
        'id_book' => 'integer',
        'id_packet' => 'integer',
        'validasi' => 'integer',
        'id_user' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
