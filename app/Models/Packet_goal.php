<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_goal",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_goal",
 *          description="id_packet_goal",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_goal",
 *          description="order_goal",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="goal",
 *          description="goal",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="icon",
 *          description="icon",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Packet_goal extends Model
{

    public $table = 'packet_goal';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_goal';

    public $fillable = [
        'id_packet',
        'order_goal',
        'goal',
        'icon',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_goal' => 'integer',
        'id_packet' => 'integer',
        'order_goal' => 'integer',
        'goal' => 'string',
        'icon' => 'string',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
