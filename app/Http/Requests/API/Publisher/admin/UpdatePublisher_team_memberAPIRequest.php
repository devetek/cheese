<?php

namespace App\Http\Requests\API\Publisher\admin;

use App\Models\Publisher_team_member;
use InfyOm\Generator\Request\APIRequest;

class UpdatePublisher_team_memberAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Publisher_team_member::$rules;
    }
}
