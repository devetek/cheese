<?php

namespace App\Http\Controllers;

use Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Home Pesantren API",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    /**
     * Send 200 HTTP response
     *
     * @param mixed $result
     * @param string $message
     * @param null $request
     * @return void
     */
    public function sendResponse($result, $message, $request = null)
    {
        return Response::json([
            'data' => $result,
            'success' => true,
            'message' => $message,
            'request' => $request,
        ]);
    }

    /**
     * Send created response
     *
     * @param mixed $data
     * @return void
     */
    public function sendCreatedResponse($data)
    {
        return response()->json($data, 201);
    }

    /**
     * Send internal server error
     *
     * @param string $message
     * @return void
     */
    public function sendErrorResponse($message)
    {
        return response()->json($message, 500);
    }

    /**
     * Send not found error response
     *
     * @param string $message
     * @return void
     */
    public function sendNotFoundResponse($message)
    {
        return response()->json($message, 404);
    }

    /**
     * Send unauthorized response
     *
     * @return void
     */
    public function sendUnauthorizedResponse()
    {
        return response()->json('Unauthorized', 401);
    }
}
