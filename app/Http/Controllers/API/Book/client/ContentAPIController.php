<?php

namespace App\Http\Controllers\API\Book\client;

use App\Models\Content;
use App\Repositories\Book\client\ContentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ContentController
 * @package book/client/ App\Http\Controllers\API\Book\client
 */

class ContentAPIController extends AppBaseController
{
    /** @var  ContentRepository */
    private $contentRepository;

    public function __construct(ContentRepository $contentRepo)
    {
        $this->contentRepository = $contentRepo;
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/client/contents/{id}",
     *      summary="Display the specified Content",
     *      tags={"Book/client/Content"},
     *      description="Get Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Content"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Content $content */
        $content = $this->contentRepository->detail($id);

        if (empty($content)) {
            return $this->sendError('Content not found');
        }

        return $this->sendResponse($content->toArray(), 'Content retrieved successfully');
    }

}
