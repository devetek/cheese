<?php

namespace App\Http\Controllers\API;

use App\Helper\Query;
use App\Http\Controllers\AppBaseController;
use App\Services\PurchaseService;
use App\User;
use Illuminate\Http\Request;

class FCM_user extends AppBaseController
{
    public function terimaUser(Request $request, $token)
    {
        if($token)
        {
            if(auth('api')->user()->id > 0)
            {
                $user = User::find(auth('api')->user()->id);
                if($user)
                {
                    $user->fcm_token = $token;
                    if($token !='[object Object]')
                    {
                        $user->save();
                    }

                }
            }
        }
        return ;
        // return Query::getUser()->id;
    }
}
