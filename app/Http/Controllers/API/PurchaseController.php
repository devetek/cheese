<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Services\PurchaseService;
use Illuminate\Http\Request;

class PurchaseController extends AppBaseController
{
    public function checkout(PurchaseService $purchase)
    {
        try {
            $purchase = $purchase->checkout();

            return $this->sendResponse(
                $purchase->toArray(),
                'Purchase saved successfully'
            );
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    public function confirm($id, Request $request, PurchaseService $purchase)
    {
        try {
            $this->validate($request, [
                'packageId' => 'required',
            ]);

            $data = [
                'id' => $id,
                'package_id' => $request->packageId,
            ];

            $purchase = $purchase->confirm($data);

            return $this->sendResponse(
                $purchase,
                'Purchase updated successfully'
            );
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
