<?php

namespace App\Http\Controllers\API\Ticket\client;

use App\Helper\Query;
use App\Models\Ticket_answer;
use App\Repositories\Ticket\client\Ticket_answerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Ticket_answerController
 * @package ticket/client/ App\Http\Controllers\API\Ticket\client
 */

class Ticket_answerAPIController extends AppBaseController
{
    /** @var  Ticket_answerRepository */
    private $ticketAnswerRepository;

    public function __construct(Ticket_answerRepository $ticketAnswerRepo)
    {
        $this->ticketAnswerRepository = $ticketAnswerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticket/client/ticketAnswers",
     *      summary="Get a listing of the Ticket_answers.",
     *      tags={"Ticket/client/Ticket_answer"},
     *      description="Get all Ticket_answers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Ticket_answer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ticketAnswers = $this->ticketAnswerRepository->all($request);
        return $this->sendResponse($ticketAnswers, 'Ticket Answers retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreateTicket_answerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ticket/client/ticketAnswers",
     *      summary="Store a newly created Ticket_answer in storage",
     *      tags={"Ticket/client/Ticket_answer"},
     *      description="Store Ticket_answer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Ticket_answer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Ticket_answer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Ticket_answer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $ticketAnswers = $this->ticketAnswerRepository->create($input);



            return $this->sendResponse($ticketAnswers, 'Ticket Answer saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticket/client/ticketAnswers/{id}",
     *      summary="Display the specified Ticket_answer",
     *      tags={"Ticket/client/Ticket_answer"},
     *      description="Get Ticket_answer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Ticket_answer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Ticket_answer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Ticket_answer $ticketAnswer */
        $ticketAnswer = $this->ticketAnswerRepository->findWithoutFail($id);

        if (empty($ticketAnswer)) {
            return $this->sendError('Ticket Answer not found');
        }

        return $this->sendResponse($ticketAnswer->toArray(), 'Ticket Answer retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Ticket_answer $ticketAnswer */
        $ticketAnswer = $this->ticketAnswerRepository->edit($id);

        if (empty($ticketAnswer)) {
            return $this->sendError('Ticket Answer not found');
        }

        return $this->sendResponse($ticketAnswer->toArray(), 'Ticket Answer retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdateTicket_answerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ticket/client/ticketAnswers/{id}",
     *      summary="Update the specified Ticket_answer in storage",
     *      tags={"Ticket/client/Ticket_answer"},
     *      description="Update Ticket_answer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Ticket_answer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Ticket_answer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Ticket_answer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Ticket_answer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Ticket_answer $ticketAnswer */
        $ticketAnswer = $this->ticketAnswerRepository->findWithoutFail($id);

        if (empty($ticketAnswer)) {
            return $this->sendError('Ticket Answer not found');
        }

        $ticketAnswer = $this->ticketAnswerRepository->update($input, $id);

        return $this->sendResponse($ticketAnswer->toArray(), 'Ticket_answer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ticket/client/ticketAnswers/{id}",
     *      summary="Remove the specified Ticket_answer from storage",
     *      tags={"Ticket/client/Ticket_answer"},
     *      description="Delete Ticket_answer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Ticket_answer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Ticket_answer $ticketAnswer */
        $ticketAnswer = $this->ticketAnswerRepository->findWithoutFail($id);

        if (empty($ticketAnswer)) {
            return $this->sendError('Ticket Answer not found');
        }

        $ticketAnswer->delete();

        return $this->sendResponse($id, 'Ticket Answer deleted successfully');
    }
}
