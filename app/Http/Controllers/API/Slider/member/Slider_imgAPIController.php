<?php

namespace App\Http\Controllers\API\Slider\member;

use App\Models\Slider_img;
use App\Repositories\Slider\member\Slider_imgRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Slider_imgController
 * @package slider/member/ App\Http\Controllers\API\Slider\member
 */

class Slider_imgAPIController extends AppBaseController
{
    /** @var  Slider_imgRepository */
    private $sliderImgRepository;

    public function __construct(Slider_imgRepository $sliderImgRepo)
    {
        $this->sliderImgRepository = $sliderImgRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/slider/member/sliderImgs",
     *      summary="Get a listing of the Slider_imgs.",
     *      tags={"Slider/member/Slider_img"},
     *      description="Get all Slider_imgs",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Slider_img")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $sliderImgs = $this->sliderImgRepository->all();
        return $this->sendResponse($sliderImgs, 'Slider Imgs retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreateSlider_imgAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/slider/member/sliderImgs",
     *      summary="Store a newly created Slider_img in storage",
     *      tags={"Slider/member/Slider_img"},
     *      description="Store Slider_img",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Slider_img that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Slider_img")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Slider_img"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $sliderImgs = $this->sliderImgRepository->create($input);

            return $this->sendResponse($sliderImgs, 'Slider Img saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/slider/member/sliderImgs/{id}",
     *      summary="Display the specified Slider_img",
     *      tags={"Slider/member/Slider_img"},
     *      description="Get Slider_img",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Slider_img",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Slider_img"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Slider_img $sliderImg */
        $sliderImg = $this->sliderImgRepository->findWithoutFail($id);

        if (empty($sliderImg)) {
            return $this->sendError('Slider Img not found');
        }

        return $this->sendResponse($sliderImg->toArray(), 'Slider Img retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdateSlider_imgAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/slider/member/sliderImgs/{id}",
     *      summary="Update the specified Slider_img in storage",
     *      tags={"Slider/member/Slider_img"},
     *      description="Update Slider_img",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Slider_img",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Slider_img that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Slider_img")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Slider_img"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Slider_img $sliderImg */
        $sliderImg = $this->sliderImgRepository->findWithoutFail($id);

        if (empty($sliderImg)) {
            return $this->sendError('Slider Img not found');
        }

        $sliderImg = $this->sliderImgRepository->update($input, $id);

        return $this->sendResponse($sliderImg->toArray(), 'Slider_img updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/slider/member/sliderImgs/{id}",
     *      summary="Remove the specified Slider_img from storage",
     *      tags={"Slider/member/Slider_img"},
     *      description="Delete Slider_img",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Slider_img",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Slider_img $sliderImg */
        $sliderImg = $this->sliderImgRepository->findWithoutFail($id);

        if (empty($sliderImg)) {
            return $this->sendError('Slider Img not found');
        }

        $sliderImg->delete();

        return $this->sendResponse($id, 'Slider Img deleted successfully');
    }
}
