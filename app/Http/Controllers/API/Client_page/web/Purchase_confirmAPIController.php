<?php

namespace App\Http\Controllers\API\Client_Page\web;

use App\Helper\Query;
use App\Models\Purchase_confirm;
use App\Repositories\Client_Page\web\Purchase_confirmRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class Purchase_confirmController
 * @package clientPage/web/ App\Http\Controllers\API\Client_Page\web
 */
class Purchase_confirmAPIController extends AppBaseController
{
    /** @var  Purchase_confirmRepository */
    private $purchaseConfirmRepository;

    public function __construct(Purchase_confirmRepository $purchaseConfirmRepo)
    {
        $this->purchaseConfirmRepository = $purchaseConfirmRepo;
    }


    /**
     * @param CreatePurchase_confirmAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clientPage/web/purchaseConfirms",
     *      summary="Store a newly created Purchase_confirm in storage",
     *      tags={"Client_Page/web/Purchase_confirm"},
     *      description="Store Purchase_confirm",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Purchase_confirm that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Purchase_confirm")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase_confirm"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $input = $request->all();

//        jika dia confirm
        if($request->input('transfer_form_chanel'))
        {

            $file = $request->file('file_td');
            if ($file) {
                $request->request->add(['foto_confirm' => $file->getClientOriginalName()]);


                $pacth = public_path() . '/confirm/' . $request->input('id_purchase_confirm') . "/";
                if (!file_exists($pacth)) {
                    mkdir($pacth, 0777, true);
                }
                if ($file->move($pacth,  $file->getClientOriginalName())) {


                    $purchaseConfirms = $this->purchaseConfirmRepository->update($request ,$request->input('id_purchase_confirm'));
                }
            } else {
                return "gsgsl";
            }

        }
        else
        {
            //        cek apakah benar
            $sql=" SELECT * 
                    FROM purchase 
                    WHERE id_user=".Query::getUser()->id." 
                    and amount_paid is null
                    and id_purchase= ".$request->input('id');
            $cek = DB::select($sql);
            if(count($cek) < 1)
            {
                return $this->sendResponse("gagal", 'Purchase gagal');
            }

            $purchaseConfirms = $this->purchaseConfirmRepository->create($input);
        }


        return $this->sendResponse($purchaseConfirms, 'Purchase Confirm saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/clientPage/web/purchaseConfirms/{id}",
     *      summary="Display the specified Purchase_confirm",
     *      tags={"Client_Page/web/Purchase_confirm"},
     *      description="Get Purchase_confirm",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase_confirm",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase_confirm"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Purchase_confirm $purchaseConfirm */
        $purchaseConfirm = $this->purchaseConfirmRepository->findWithoutFail($id);

        if (empty($purchaseConfirm)) {
            return $this->sendError('Purchase Confirm not found');
        }

        return $this->sendResponse($purchaseConfirm->toArray(), 'Purchase Confirm retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Purchase_confirm $purchaseConfirm */
        $purchaseConfirm = $this->purchaseConfirmRepository->edit($id);

        if (empty($purchaseConfirm)) {
            return $this->sendError('Purchase Confirm not found');
        }

        return $this->sendResponse($purchaseConfirm->toArray(), 'Purchase Confirm retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePurchase_confirmAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/clientPage/web/purchaseConfirms/{id}",
     *      summary="Update the specified Purchase_confirm in storage",
     *      tags={"Client_Page/web/Purchase_confirm"},
     *      description="Update Purchase_confirm",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase_confirm",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Purchase_confirm that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Purchase_confirm")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase_confirm"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Purchase_confirm $purchaseConfirm */
        $purchaseConfirm = $this->purchaseConfirmRepository->findWithoutFail($id);

        if (empty($purchaseConfirm)) {
            return $this->sendError('Purchase Confirm not found');
        }

        $purchaseConfirm = $this->purchaseConfirmRepository->update($input, $id);

        return $this->sendResponse($purchaseConfirm->toArray(), 'Purchase_confirm updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clientPage/web/purchaseConfirms/{id}",
     *      summary="Remove the specified Purchase_confirm from storage",
     *      tags={"Client_Page/web/Purchase_confirm"},
     *      description="Delete Purchase_confirm",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase_confirm",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Purchase_confirm $purchaseConfirm */
        $purchaseConfirm = $this->purchaseConfirmRepository->findWithoutFail($id);

        if (empty($purchaseConfirm)) {
            return $this->sendError('Purchase Confirm not found');
        }

        $purchaseConfirm->delete();

        return $this->sendResponse($id, 'Purchase Confirm deleted successfully');
    }
}
