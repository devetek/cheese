<?php

namespace App\Http\Controllers\API\Client_page\mobile_guest;

use App\Repositories\Client_page\mobile_guest\PacketRequirementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;



class PacketRequirementController extends AppBaseController
{
    /** @var  BookRepository */
    private $packetRequirementRepository;

    public function __construct(PacketRequirementRepository $packetRequirementRepo)
    {
        $this->packetRequirementRepository = $packetRequirementRepo;
    }

    public function show(Request $request,$id_packet)
    {
        $books = $this->packetRequirementRepository->detail($id_packet);
        return $this->sendResponse($books, 'Packet retrieved successfully');
    }

}
