<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_detail;
use App\Repositories\Packet\admin\Packet_detailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_detailController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_detailAPIController extends AppBaseController
{
    /** @var  Packet_detailRepository */
    private $packetDetailRepository;

    public function __construct(Packet_detailRepository $packetDetailRepo)
    {
        $this->packetDetailRepository = $packetDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetDetails",
     *      summary="Get a listing of the Packet_details.",
     *      tags={"Packet/admin/Packet_detail"},
     *      description="Get all Packet_details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_detail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetDetails = $this->packetDetailRepository->all($request);
        return $this->sendResponse($packetDetails, 'Packet Details retrieved successfully');
    }

    /**
     * @param CreatePacket_detailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetDetails",
     *      summary="Store a newly created Packet_detail in storage",
     *      tags={"Packet/admin/Packet_detail"},
     *      description="Store Packet_detail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_detail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_detail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_detail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetDetails = $this->packetDetailRepository->create($input);

            return $this->sendResponse($packetDetails, 'Packet Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetDetails/{id}",
     *      summary="Remove the specified Packet_detail from storage",
     *      tags={"Packet/admin/Packet_detail"},
     *      description="Delete Packet_detail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_detail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_detail $packetDetail */
        $packetDetail = $this->packetDetailRepository->findWithoutFail($id);

        if (empty($packetDetail)) {
            return $this->sendError('Packet Detail not found');
        }

        $packetDetail->delete();

        return $this->sendResponse($id, 'Packet Detail deleted successfully');
    }
}
