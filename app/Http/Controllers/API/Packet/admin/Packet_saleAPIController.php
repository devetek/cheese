<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_sale;
use App\Repositories\Packet\admin\Packet_saleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_saleController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_saleAPIController extends AppBaseController
{
    /** @var  Packet_saleRepository */
    private $packetSaleRepository;

    public function __construct(Packet_saleRepository $packetSaleRepo)
    {
        $this->packetSaleRepository = $packetSaleRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetSales",
     *      summary="Get a listing of the Packet_sales.",
     *      tags={"Packet/admin/Packet_sale"},
     *      description="Get all Packet_sales",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_sale")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetSales = $this->packetSaleRepository->all($request);
        return $this->sendResponse($packetSales, 'Packet Sales retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePacket_saleAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetSales",
     *      summary="Store a newly created Packet_sale in storage",
     *      tags={"Packet/admin/Packet_sale"},
     *      description="Store Packet_sale",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_sale that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_sale")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_sale"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetSales = $this->packetSaleRepository->create($input);

            return $this->sendResponse($packetSales, 'Packet Sale saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetSales/{id}",
     *      summary="Display the specified Packet_sale",
     *      tags={"Packet/admin/Packet_sale"},
     *      description="Get Packet_sale",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_sale",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_sale"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet_sale $packetSale */
        $packetSale = $this->packetSaleRepository->findWithoutFail($id);

        if (empty($packetSale)) {
            return $this->sendError('Packet Sale not found');
        }

        return $this->sendResponse($packetSale->toArray(), 'Packet Sale retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Packet_sale $packetSale */
        $packetSale = $this->packetSaleRepository->edit($id);

        if (empty($packetSale)) {
            return $this->sendError('Packet Sale not found');
        }

        return $this->sendResponse($packetSale->toArray(), 'Packet Sale retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePacket_saleAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/packet/admin/packetSales/{id}",
     *      summary="Update the specified Packet_sale in storage",
     *      tags={"Packet/admin/Packet_sale"},
     *      description="Update Packet_sale",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_sale",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_sale that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_sale")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_sale"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Packet_sale $packetSale */
        $packetSale = $this->packetSaleRepository->findWithoutFail($id);

        if (empty($packetSale)) {
            return $this->sendError('Packet Sale not found');
        }

        $packetSale = $this->packetSaleRepository->update($input, $id);

        return $this->sendResponse($packetSale->toArray(), 'Packet_sale updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetSales/{id}",
     *      summary="Remove the specified Packet_sale from storage",
     *      tags={"Packet/admin/Packet_sale"},
     *      description="Delete Packet_sale",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_sale",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_sale $packetSale */
        $packetSale = $this->packetSaleRepository->findWithoutFail($id);

        if (empty($packetSale)) {
            return $this->sendError('Packet Sale not found');
        }

        $packetSale->delete();

        return $this->sendResponse($id, 'Packet Sale deleted successfully');
    }
}
