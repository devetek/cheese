<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_requirement;
use App\Repositories\Packet\admin\Packet_requirementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_requirementController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_requirementAPIController extends AppBaseController
{
    /** @var  Packet_requirementRepository */
    private $packetRequirementRepository;

    public function __construct(Packet_requirementRepository $packetRequirementRepo)
    {
        $this->packetRequirementRepository = $packetRequirementRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetRequirements",
     *      summary="Get a listing of the Packet_requirements.",
     *      tags={"Packet/admin/Packet_requirement"},
     *      description="Get all Packet_requirements",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_requirement")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetRequirements = $this->packetRequirementRepository->all($request);
        return $this->sendResponse($packetRequirements, 'Packet Requirements retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePacket_requirementAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetRequirements",
     *      summary="Store a newly created Packet_requirement in storage",
     *      tags={"Packet/admin/Packet_requirement"},
     *      description="Store Packet_requirement",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_requirement that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_requirement")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_requirement"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetRequirements = $this->packetRequirementRepository->create($input);

            return $this->sendResponse($packetRequirements, 'Packet Requirement saved successfully');
    }



    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetRequirements/{id}",
     *      summary="Remove the specified Packet_requirement from storage",
     *      tags={"Packet/admin/Packet_requirement"},
     *      description="Delete Packet_requirement",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_requirement",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_requirement $packetRequirement */
        $packetRequirement = $this->packetRequirementRepository->findWithoutFail($id);

        if (empty($packetRequirement)) {
            return $this->sendError('Packet Requirement not found');
        }

        $packetRequirement->delete();

        return $this->sendResponse($id, 'Packet Requirement deleted successfully');
    }
}
