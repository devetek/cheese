<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_book;
use App\Repositories\Packet\admin\Packet_bookRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_bookController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_bookAPIController extends AppBaseController
{
    /** @var  Packet_bookRepository */
    private $packetBookRepository;

    public function __construct(Packet_bookRepository $packetBookRepo)
    {
        $this->packetBookRepository = $packetBookRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetBooks",
     *      summary="Get a listing of the Packet_books.",
     *      tags={"Packet/admin/Packet_book"},
     *      description="Get all Packet_books",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_book")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetBooks = $this->packetBookRepository->all($request);
        return $this->sendResponse($packetBooks, 'Packet Books retrieved successfully');
    }

    public function create(Request $request)
    {
        $packetBooks = $this->packetBookRepository->pree($request);
        return $this->sendResponse($packetBooks, 'Packet Books retrieved successfully');
    }

    /**
     * @param CreatePacket_bookAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetBooks",
     *      summary="Store a newly created Packet_book in storage",
     *      tags={"Packet/admin/Packet_book"},
     *      description="Store Packet_book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_book that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_book")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetBooks = $this->packetBookRepository->create($request, $input);

            return $this->sendResponse($packetBooks, 'Packet Book saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetBooks/{id}",
     *      summary="Display the specified Packet_book",
     *      tags={"Packet/admin/Packet_book"},
     *      description="Get Packet_book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_book",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet_book $packetBook */
        $packetBook = $this->packetBookRepository->findWithoutFail($id);

        if (empty($packetBook)) {
            return $this->sendError('Packet Book not found');
        }

        return $this->sendResponse($packetBook->toArray(), 'Packet Book retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Packet_book $packetBook */
        $packetBook = $this->packetBookRepository->edit($id);

        if (empty($packetBook)) {
            return $this->sendError('Packet Book not found');
        }

        return $this->sendResponse($packetBook->toArray(), 'Packet Book retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePacket_bookAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/packet/admin/packetBooks/{id}",
     *      summary="Update the specified Packet_book in storage",
     *      tags={"Packet/admin/Packet_book"},
     *      description="Update Packet_book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_book",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_book that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_book")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Packet_book $packetBook */
        $packetBook = $this->packetBookRepository->findWithoutFail($id);

        if (empty($packetBook)) {
            return $this->sendError('Packet Book not found');
        }

        $packetBook = $this->packetBookRepository->update($input, $id);

        return $this->sendResponse($packetBook->toArray(), 'Packet_book updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetBooks/{id}",
     *      summary="Remove the specified Packet_book from storage",
     *      tags={"Packet/admin/Packet_book"},
     *      description="Delete Packet_book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_book",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_book $packetBook */
        $packetBook = $this->packetBookRepository->findWithoutFail($id);

        if (empty($packetBook)) {
            return $this->sendError('Packet Book not found');
        }

        $packetBook->delete();

        return $this->sendResponse($id, 'Packet Book deleted successfully');
    }
}
