<?php

namespace App\Http\Controllers\API\Packet\admin;
 
use App\Repositories\Packet\admin\Packet_coverRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_coverController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_coverAPIController extends AppBaseController
{
    /** @var  Packet_coverRepository */
    private $packetCoverRepository;

    public function __construct(Packet_coverRepository $packetCoverRepo)
    {
        $this->packetCoverRepository = $packetCoverRepo;
    }


    /**
     * @param CreatePacket_coverAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetCovers",
     *      summary="Store a newly created Packet_cover in storage",
     *      tags={"Packet/admin/Packet_cover"},
     *      description="Store Packet_cover",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_cover that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetCovers = $this->packetCoverRepository->create($request,$input);

            return $this->sendResponse($packetCovers, 'Packet Cover saved successfully');
    }

}
