<?php

namespace App\Http\Controllers\API\Company\admin;

use App\Models\Company;
use App\Repositories\Company\admin\CompanyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CompanyController
 * @package company/admin/ App\Http\Controllers\API\Company\admin
 */

class CompanyAPIController extends AppBaseController
{
    /** @var  CompanyRepository */
    private $companyRepository;

    public function __construct(CompanyRepository $companyRepo)
    {
        $this->companyRepository = $companyRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *   path="/company/admin/companies",
     *   summary="Get a listing of the Companies.",
     *   tags={"Company/admin/Company"},
     *   description="Get all Companies",
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="success",
     *         type="boolean"
     *       ),
     *       @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Company")
     *       ),
     *       @SWG\Property(
     *         property="message",
     *         type="string"
     *       )
     *     )
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Invalid server error"
     *   )
     * )
     */
    public function index(Request $request)
    {
        $companies = $this->companyRepository->all($request);
        return $this->sendResponse($companies, 'Companies retrieved successfully');
    }
    
    public function edit(Request $reques, $id)
    {
        $data = Company::find($id);
        return $this->sendResponse($data, 'Companies retrieved successfully');
    }

    /**
     * @param CreateCompanyAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *   path="/company/admin/companies",
     *   summary="Store a newly created Company in storage",
     *   tags={"Company/admin/Company"},
     *   description="Store Company",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Company that should be stored",
     *     required=false,
     *     @SWG\Schema(ref="#/definitions/Company")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="success",
     *         type="boolean"
     *       ),
     *       @SWG\Property(
     *         property="data",
     *         ref="#/definitions/Company"
     *       ),
     *       @SWG\Property(
     *         property="message",
     *         type="string"
     *       )
     *     )
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Invalid server error.",
     *   )
     * )
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $companies = $this->companyRepository->create($input);

        return $this->sendResponse($companies, 'Company saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *   path="/company/admin/companies/{id}",
     *   summary="Display the specified Company",
     *   tags={"Company/admin/Company"},
     *   description="Get Company",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id",
     *     description="id of Company",
     *     type="integer",
     *     required=true,
     *     in="path"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="success",
     *         type="boolean"
     *       ),
     *       @SWG\Property(
     *         property="data",
     *         ref="#/definitions/Company"
     *       ),
     *       @SWG\Property(
     *          property="message",
     *          type="string"
     *       )
     *     )
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="Company not found"
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Invalid server error"
     *   )
     * )
     */
    public function show($id)
    {
        /** @var Company $company */
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            return $this->sendNotFoundResponse('Company not found');
        }

        return $this->sendResponse($company->toArray(), 'Company retrieved successfully');
    }


    /**
     * @SWG\Put(
     *    path="/company/admin/companies/{id}",
     *    summary="Update the specified Company in storage",
     *    tags={"Company/admin/Company"},
     *    description="Update Company",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *      name="id",
     *      description="id of Company",
     *      type="integer",
     *      required=true,
     *      in="path"
     *    ),
     *    @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      description="Company that should be updated",
     *      required=false,
     *      @SWG\Schema(ref="#/definitions/Company")
     *    ),
     *    @SWG\Response(
     *      response=200,
     *      description="Successful operation",
     *      @SWG\Schema(
     *        type="object",
     *        @SWG\Property(
     *          property="success",
     *          type="boolean"
     *        ),
     *        @SWG\Property(
     *          property="data",
     *          ref="#/definitions/Company"
     *        ),
     *        @SWG\Property(
     *          property="message",
     *          type="string"
     *        )
     *      )
     *    ),
     *    @SWG\Response(
     *      response=404,
     *      description="Company not found"
     *    ),
     *    @SWG\Response(
     *     response=500,
     *     description="Invalid server error"
     *   )
     *  )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            return $this->sendNotFoundResponse('Company not found');
        }

        $company = $this->companyRepository->update($input, $id);

        return $this->sendResponse($company->toArray(), 'Company updated successfully');
    }

    /**
     * @SWG\Delete(
     *    path="/company/admin/companies/{id}",
     *    summary="Remove the specified Company from storage",
     *    tags={"Company/admin/Company"},
     *    description="Delete Company",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *      name="id",
     *      description="id of Company",
     *      type="integer",
     *      required=true,
     *      in="path"
     *    ),
     *    @SWG\Response(
     *      response=200,
     *      description="Successful operation",
     *      @SWG\Schema(
     *        type="object",
     *        @SWG\Property(
     *          property="success",
     *          type="boolean"
     *        ),
     *        @SWG\Property(
     *          property="data",
     *          type="string"
     *        ),
     *        @SWG\Property(
     *          property="message",
     *          type="string"
     *        )
     *      )
     *    ),
     *    @SWG\Response(
     *      response=404,
     *      description="Company not found",
     *    ),
     *    @SWG\Response(
     *     response=500,
     *     description="Invalid server error"
     *   )
     *  )
     */
    public function destroy($id)
    {
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            return $this->sendNotFoundResponse('Company not found');
        }

        $company->delete();

        return $this->sendResponse($id, 'Company deleted successfully');
    }
}
