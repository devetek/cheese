<?php
namespace App\Libraries;

use Illuminate\Http\Request;
use Illuminate\Routing\RouteCollection;
use Illuminate\Routing\UrlGenerator;

class CustomUrlGenerator extends UrlGenerator
{
  private $baseUrl = '';

  public function __construct(RouteCollection $routes, Request $request)
  {
    parent::__construct($routes, $request);

    $this->baseUrl = env('APP_URL');
  }

  /**
   * Inheritance method from Illuminate\Routing\UrlGenerator.
   *
   * @return string
   */
  public function formatRoot($scheme, $root = null)
  {
    return parent::formatRoot($scheme, $this->baseUrl());
  }

  /**
   * Generate baseUrl from APP_URL environment variable.
   *
   * @return string
   */
  private function baseUrl() {
    $parseUrl = parse_url($this->baseUrl);
    $hasPort = @$parseUrl['port'] ? ':' . @$parseUrl['port'] : '';
    
    return @$parseUrl['scheme'] . '://' . @$parseUrl['host'] . $hasPort;
  }
}
