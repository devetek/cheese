<?php

namespace App\V1\Contracts;

interface CompanyServiceContract
{
    public function get();
    public function find($id);
}
