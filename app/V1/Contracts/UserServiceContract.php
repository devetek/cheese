<?php

namespace App\V1\Contracts;

interface UserServiceContract
{
    public function find($id);
    public function update($id, array $data);
    public function changePassword($id, array $data);
}
