<?php

namespace App\V1\Contracts;

interface PublisherServiceContract
{
    public function get();
    public function find($id);
}
