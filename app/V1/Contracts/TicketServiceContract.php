<?php

namespace App\V1\Contracts;

interface TicketServiceContract
{
    public function create(array $data);
    public function find($id);
    public function getAnswers($id);
}
