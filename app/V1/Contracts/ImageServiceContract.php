<?php

namespace App\V1\Contracts;

interface ImageServiceContract
{
    public function upload($path, $file);
}
