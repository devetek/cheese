<?php

namespace App\V1\Contracts;

interface PurchaseServiceContract
{
    public function create(array $data);
    public function confirm($id, array $data);
    public function getUnpaidPurchase($user_id);
}
