<?php

namespace App\V1\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReturningBackUniqueCode
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $unique_code;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($unique_code)
    {
        $this->unique_code = $unique_code;
    }

    /**
     * Return unique code value
     *
     * @return void
     */
    public function getUnicode()
    {
        return $this->unique_code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
