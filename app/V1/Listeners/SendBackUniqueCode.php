<?php

namespace App\Listeners;

use App\V1\Events\ReturningBackUniqueCode;
use GuzzleHttp\Client;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBackUniqueCode
{
    protected $client;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ReturningBackUniqueCode $event)
    {
        $unique_code = $event->getUnicode();
        $this->client->request('PUT', 'v2/payment/unicode/' . $unique_code);
    }
}
