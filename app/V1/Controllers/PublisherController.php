<?php

namespace App\V1\Controllers;

use App\Http\Controllers\AppBaseController as Controller;
use App\V1\Exceptions\PublisherNotFoundException;
use App\V1\Services\PublisherService;

class PublisherController extends Controller
{
    /**
     * @var PublisherService
     */
    protected $service;

    /**
     * PublisherController construct
     *
     * @param PublisherService $service
     */
    public function __construct(PublisherService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *  path="/v1/publishers",
     *  summary="Get a listing of the publishers.",
     *  tags={"v1/Publishers"},
     *  description ="Get all publishers",
     *  produces={"application/json"},
     *  @SWG\Response(
     *    response=200,
     *    description="successful operation",
     *    @SWG\Schema(
     *      type="object",
     *      @SWG\Property(
     *        property="success",
     *        type="boolean"
     *      ),
     *      @SWG\Property(
     *        property="data",
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Publisher")
     *      ),
     *      @SWG\Property(
     *        property="message",
     *        type="string"
     *      )
     *    )
     *  ),
     *  @SWG\Response(
     *    response=500,
     *    description="Internal server error",
     *  ),
     * )
     *
     * @return Response
     */
    public function get()
    {
        try {
            $publisher_list = $this->service->get();

            return $this->sendResponse($publisher_list, 'Publisher successfuly retrieved');
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /** @SWG\Get(
     *  path="/v1/publishers/{id}",
     *  summary="Get a publisher by ID.",
     *  tags={"v1/Publishers"},
     *  description ="Get a publisher",
     *  produces={"application/json"},
     *  @SWG\Parameter(
     *    name="id",
     *    description="Publisher ID",
     *    required=true,
     *    type="integer",
     *    in="path"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Successful operation",
     *    @SWG\Schema(
     *      type="object",
     *      @SWG\Property(
     *        property="success",
     *        type="boolean"
     *      ),
     *      @SWG\Property(
     *        property="data",
     *        ref="#/definitions/Publisher"
     *      ),
     *      @SWG\Property(
     *        property="message",
     *        type="string"
     *      )
     *    )
     *  ),
     *  @SWG\Response(
     *    response=404,
     *    description="Publisher not found",
     *  ),
     *  @SWG\Response(
     *    response=500,
     *    description="Internal server error",
     *  ),
     * )
     *
     * @param int $id
     * @return Response
     */
    public function find($id)
    {
        try {
            $publisher = $this->service->find($id);

            return $this->sendResponse($publisher, 'Publisher successfully retrieved');
        } catch (PublisherNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *  path="/v1/publishers/teams",
     *  summary="Get a listing of the publisher teams.",
     *  tags={"v1/Publishers"},
     *  description ="Get all publisher teams",
     *  produces={"application/json"},
     *  @SWG\Response(
     *    response=200,
     *    description="successful operation",
     *    @SWG\Schema(
     *      type="object",
     *      @SWG\Property(
     *        property="success",
     *        type="boolean"
     *      ),
     *      @SWG\Property(
     *        property="data",
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Publisher_team")
     *      ),
     *      @SWG\Property(
     *        property="message",
     *        type="string"
     *      )
     *    )
     *  ),
     *  @SWG\Response(
     *    response=500,
     *    description="Internal server error",
     *  ),
     * )
     *
     * @return void
     */
    public function getTeams()
    {
        try {
            $teams = $this->service->getTeams();

            return $this->sendResponse($teams, 'Publisher team successfully retrieved');
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /** @SWG\Get(
     *  path="/v1/publishers/{id}/teams",
     *  summary="Get a publisher team by ID.",
     *  tags={"v1/Publishers"},
     *  description ="Get a publisher teams",
     *  produces={"application/json"},
     *  @SWG\Parameter(
     *    name="id",
     *    description="Publisher ID",
     *    required=true,
     *    type="integer",
     *    in="path"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Successful operation",
     *    @SWG\Schema(
     *      type="object",
     *      @SWG\Property(
     *        property="success",
     *        type="boolean"
     *      ),
     *      @SWG\Property(
     *        property="data",
     *        ref="#/definitions/Publisher_team"
     *      ),
     *      @SWG\Property(
     *        property="message",
     *        type="string"
     *      )
     *    )
     *  ),
     *  @SWG\Response(
     *    response=404,
     *    description="Publisher not found",
     *  ),
     *  @SWG\Response(
     *    response=500,
     *    description="Internal server error",
     *  ),
     * )
     *
     * @param int $id
     * @return void
     */
    public function findTeams($id)
    {
        try {
            $publisher_teams = $this->service->findTeams($id);

            return $this->sendResponse($publisher_teams, 'Publisher team successfully retrieved');
        } catch (PublisherNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
