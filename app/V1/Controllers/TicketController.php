<?php

namespace App\V1\Controllers;

use App\Http\Controllers\AppBaseController as Controller;
use App\V1\Exceptions\TicketNotFoundException;
use App\V1\Services\TicketService;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;

class TicketController extends Controller
{
    /**
     * @var TicketService
     */
    protected $service;

    /**
     * TicketController construct.
     *
     * @param TicketService $service
     */
    public function __construct(TicketService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *   path="/v1/tickets/{submodule_id}",
     *   description="Return all tickets of submodule",
     *   summary="Return all tickets of submodule",
     *   tags={"v1/Tickets"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="submodule_id",
     *     type="integer",
     *     required=true
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error",
     *   )
     * )
     *
     * @param int $submodule_id
     * @return void
     */
    public function get($submodule_id)
    {
        try {
            $tickets = $this->service->get($submodule_id);

            return $this->sendResponse($tickets, 'Tickets succesfully retrieved');
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *   path="v1/tickets/{id}",
     *   description="Get single ticket",
     *   summary="Get single ticket",
     *   tags={"v1/Tickets"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="Ticket not found",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error"
     *   )
     * )
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $ticket = $this->service->find($id);

            return $this->sendResponse($ticket, 'Tickets successfully retrieved');
        } catch (TicketNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *   path="v1/tickets",
     *   description="Store a new ticket",
     *   summary="Store a new ticket",
     *   tags={"v1/Tickets"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="formData",
     *     name="submodule_id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="package_id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="title",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="body",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="Successfuly created",
     *   ),
     *    @SWG\Response(
     *     response=500,
     *     description="Internal server error",
     *   )
     * )
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'submodule_id' => 'required',
                'package_id' => 'required',
                'title_ticket' => 'required',
                'ticket_content' => 'required',
            ]);

            $data = [
                'sub_module_id' => $request->submodule_id,
                'module_id' => $request->package_id,
                'title_ticket' => $request->title_ticket,
                'ticket_content' => $request->ticket_content,
                'id_user' => $request->user()->id,
            ];

            $ticket = $this->service->create($data);

            return $this->sendCreatedResponse($ticket);
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *   path="v1/tickets/{id}/answers",
     *   description="Get all answers by a ticket",
     *   summary="Get all answers by a ticket",
     *   tags={"v1/Tickets"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error",
     *   )
     * )
     *
     * @param int $id
     * @return void
     */
    public function getAnswers($id)
    {
        try {
            $answers = $this->service->getAnswers($id);

            return $this->sendResponse($answers, 'Answers of ticket successfully retrieved');
        } catch (TicketNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
