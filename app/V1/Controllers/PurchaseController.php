<?php

namespace App\V1\Controllers;

use App\Http\Controllers\AppBaseController;
use App\V1\Exceptions\PurchaseNotFoundException;
use App\V1\Exceptions\LessPaymentException;
use App\V1\Services\PurchaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PurchaseController extends AppBaseController
{
    /**
     * @var PurchaseService
     */
    protected $purchase_service;

    /**
     * PurchaseController constructor
     *
     * @param PurchaseService $purchase_service
     */
    public function __construct(PurchaseService $purchase_service)
    {
        $this->purchase_service = $purchase_service;
    }

    /** @SWG\Post(
     *    path="/v1/purchases",
     *    summary="Store a new purchase",
     *    tags={"v1/Purchases"},
     *    description="Store a new purchase",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *      in="formData",
     *      type="integer",
     *      name="sale_package_id",
     *      required=true,
     *    ),
     *    @SWG\Response(
     *      response=201,
     *      description="Successful"
     *    ),
     *    @SWG\Response(
     *      response=500,
     *      description="Internal server error"
     *    ),
     *  )
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function purchase(Request $request)
    {
        $this->validate($request, [
            'sale_package_id' => 'required',
        ]);

        try {
            $purchase = $this->purchase_service->create([
                'sale_package_id' => $request->sale_package_id,
            ]);

            return $this->sendCreatedResponse($purchase);
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /** @SWG\Post(
     *    path="/v1/purchases/{id}/confirm-payment",
     *    summary="Send payment confirmation of a purchase",
     *    tags={"v1/Purchases"},
     *    description="Send payment confirmation of a purchase",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *      in="path",
     *      name="id",
     *      type="integer",
     *      required=true
     *    ),
     *    @SWG\Parameter(
     *      in="formData",
     *      name="transferred_form_channel",
     *      type="string",
     *      required=true
     *    ),
     *    @SWG\Parameter(
     *      in="formData",
     *      name="transferred_form_name",
     *      type="string",
     *      required=true
     *    ),
     *    @SWG\Parameter(
     *      in="formData",
     *      name="amount_paid",
     *      type="integer",
     *      required=true
     *    ),
     *    @SWG\Parameter(
     *      in="formData",
     *      name="file",
     *      type="file",
     *      required=true
     *    ),
     *    @SWG\Response(
     *      response=200,
     *      description="Successful"
     *    ),
     *    @SWG\Response(
     *      response=404,
     *      description="Purchase not found"
     *    ),
     *    @SWG\Response(
     *      response=500,
     *      description="Internal server error"
     *    ),
     *  )
     *
     * @param Request $request
     * @param $id
     * @return void
     */
    public function confirm(Request $request, $id)
    {
        $this->validate($request, [
            'transferred_form_channel' => 'required',
            'transferred_form_name' => 'required',
            'amount_paid' => 'required',
            'file' => 'required',
        ]);

        try {
            $data = [
                'transferred_form_channel' => $request->transferred_form_channel,
                'transferred_form_name' => $request->transferred_form_name,
                'amount_paid' => $request->amount_paid,
                'file' => $request->file,
            ];

            $purchase = $this->purchase_service->confirm($id, $data);

            return $this->sendResponse($purchase, 'Purchase succesfully confirmed!');
        } catch (PurchaseNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (LessPaymentException $e) {
            return $this->sendErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $this->purchase_service->delete($id);

            return $this->sendResponse([], 'success');
        } catch (PurchaseNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
