<?php

namespace App\V1\Controllers;

use App\Http\Controllers\AppBaseController as Controller;
use App\V1\Exceptions\CompanyNotFoundException;
use App\V1\Services\CompanyService;

class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    protected $company_service;

    /**
     * CompanyController constructor.
     *
     * @param CompanyService $company_service
     */
    public function __construct(CompanyService $company_service)
    {
        $this->company_service = $company_service;
    }

    /**
     * @SWG\Get(
     *  path="/v1/companies",
     *  summary="Get a listing of the Companies.",
     *  tags={"v1/Companies"},
     *  description ="Get all companies",
     *  produces={"application/json"},
     *  @SWG\Response(
     *    response=200,
     *    description="successful operation",
     *    @SWG\Schema(
     *      type="object",
     *      @SWG\Property(
     *        property="success",
     *        type="boolean"
     *      ),
     *      @SWG\Property(
     *        property="data",
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Company")
     *      ),
     *      @SWG\Property(
     *        property="message",
     *        type="string"
     *      )
     *    )
     *  ),
     *  @SWG\Response(
     *    response=500,
     *    description="Internal server error",
     *  ),
     * )
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $company_list = $this->company_service->get();

            return $this->sendResponse($company_list, 'Companies retrieved successfully');
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *  path="/v1/companies/{id}",
     *  summary="Get a company by ID.",
     *  tags={"v1/Companies"},
     *  description ="Get a company",
     *  produces={"application/json"},
     *  @SWG\Parameter(
     *    name="id",
     *    description="Company ID",
     *    required=true,
     *    type="integer",
     *    in="path"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Successful operation",
     *    @SWG\Schema(
     *      type="object",
     *      @SWG\Property(
     *        property="success",
     *        type="boolean"
     *      ),
     *      @SWG\Property(
     *        property="data",
     *        ref="#/definitions/Company"
     *      ),
     *      @SWG\Property(
     *        property="message",
     *        type="string"
     *      )
     *    )
     *  ),
     *  @SWG\Response(
     *    response=404,
     *    description="Company not found",
     *  ),
     *  @SWG\Response(
     *    response=500,
     *    description="Internal server error",
     *  ),
     * )
     *
     * @param $id
     * @return Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $company = $this->company_service->find($id);

            return $this->sendResponse($company, 'Company retrieved successfully');
        } catch (CompanyNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
