<?php

namespace App\V1\Controllers;

use App\Http\Controllers\AppBaseController as Controller;
use App\V1\Services\UserService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $user_service;

    /**
     * UserController construct.
     *
     * @param UserService $user_service
     */
    public function __construct(UserService $user_service)
    {
        $this->user_service = $user_service;
    }

    /**
     * @SWG\Get(
     *   path="v1/users/{id}",
     *   description="Find a single user by ID.",
     *   summary="Find a single user by ID.",
     *   tags={"v1/Users"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation.",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="User not found.",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error.",
     *   )
     * )
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $user = $this->user_service->find($id);

            return $this->sendResponse($user, 'User successfully retrieved');
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Patch(
     *   path="v1/users/{id}",
     *   description="Update an user account.",
     *   summary="Update an user account.",
     *   tags={"v1/Users"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="name",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="email",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation.",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="User not found.",
     *   ),
     *   @SWG\Response(
     *     response=422,
     *     description="Unprocessable entity.",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error.",
     *   ),
     * )
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        try {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
            ];

            $user = $this->user_service->update($id, $data);

            return $this->sendResponse($user, 'User profile successfully updated!');
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Patch(
     *   path="v1/users/{id}/change-password",
     *   description="Update an user password.",
     *   summary="Update an user password.",
     *   tags={"v1/Users"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="currentPassword",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="newPassword",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="confPassword",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation.",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="User not found.",
     *   ),
     *   @SWG\Response(
     *     response=422,
     *     description="Unprocessable entity.",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error.",
     *   ),
     * )
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function changePassword($id, Request $request)
    {
        $this->validate($request, [
            'currentPassword' => 'required',
            'newPassword' => 'required',
            'confPassword' => 'required|same:newPassword',
        ]);

        try {
            $data = [
                'password' => $request->newPassword,
                'current_password' => $request->currentPassword,
            ];
            
            $user = $this->user_service->changePassword($id, $data);

            return $this->sendResponse($user, 'Password successfully changed!');
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
