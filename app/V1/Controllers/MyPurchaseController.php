<?php

namespace App\V1\Controllers;

use App\Http\Controllers\AppBaseController as Controller;
use App\V1\Services\PurchaseService;
use Illuminate\Support\Facades\Auth;

class MyPurchaseController extends Controller
{
    /**
     * @var PurchaseService
     */
    protected $purchase_service;

    /**
     * MyPurchaseController construct.
     *
     * @param PurchaseService $purchase_service
     */
    public function __construct(PurchaseService $purchase_service)
    {
        $this->purchase_service = $purchase_service;
    }

    /**
     * @SWG\Get(
     *   path="/v1/my-purchases/unpaid",
     *   summary="Get unpaid purchases by an authenticated user.",
     *   tags={"v1/MyPurchases"},
     *   description ="Get unpaid purchases by an authenticated user.",
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=200,
     *     description="Successsful",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal server error",
     *   ),
     *  )
     */
    public function unpaid()
    {
        try {
            $user_id = Auth::user()->id;
            $purchases = $this->purchase_service->getUnpaidPurchase($user_id);

            return $this->sendResponse($purchases, 'Purchase successfully retrieved');
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }
    }
}
