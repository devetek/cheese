<?php

namespace App\V1\Exceptions;

class LessPaymentException extends \Exception
{
    protected $message = 'Your payment less than amount of money to be paid!';
}
