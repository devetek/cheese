<?php

namespace App\V1\Exceptions;

class UserNotFoundException extends \Exception
{
    protected $message = 'User not found';
}
