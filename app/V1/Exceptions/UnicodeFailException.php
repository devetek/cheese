<?php

namespace App\V1\Exceptions;

class UnicodeFailException extends \Exception
{
    protected $message = 'Fail to generate unique code';
}
