<?php

namespace App\V1\Exceptions;

class PublisherNotFoundException extends \Exception
{
    protected $message = 'Publisher not found!';

    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->message = 'Publisher with ID: ' . $id . ' not found';
        }
    }
}
