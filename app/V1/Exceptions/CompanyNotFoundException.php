<?php

namespace App\V1\Exceptions;

class CompanyNotFoundException extends \Exception
{
    protected $message = 'Company not found';

    public function __construct($id = null)
    {
        if (! is_null($id)) {
            $this->message = 'Company with ID ' . $id . ' not found';
        }
    }
}
