<?php

namespace App\V1\Exceptions;

class TicketNotFoundException extends \Exception
{
    protected $message = 'Ticket not found!';
}
