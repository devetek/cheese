<?php

namespace App\V1\Exceptions;

class OldPasswordNotMatchException extends \Exception
{
    protected $message = 'Your current password doesnt match with your old password';
}
