<?php

namespace App\V1\Exceptions;

class PurchaseNotFoundException extends \Exception
{
    protected $message = 'Purchase not found';

    public function __construct($id = null)
    {
        if (! $id) {
            return;
        }

        $this->message = 'Purchase ' . $id . ' not found';
    }
}
