<?php

namespace App\V1\Services;

use App\V1\Contracts\PublisherServiceContract;
use App\Models\Publisher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\V1\Exceptions\PublisherNotFoundException;
use App\Models\Publisher_team;
use Illuminate\Support\Facades\URL;

class PublisherService implements PublisherServiceContract
{
    /**
     * @var string
     */
    private $default_img;

    /**
     * @var string
     */
    private $default_thumb;

    public function __construct()
    {
        $this->default_img = URL::to('/') . "/app/publisher/thum_default.png";
        $this->default_thumb = URL::to('/') . "/app/publisher/thum_default.png";
    }

    /**
     * Return all publisher available
     *
     * @return void
     */
    public function get()
    {
        $items = Publisher::where('validasi', 1)->get();

        $publishers = $items->map(function ($item) {
            $image = URL::to('/') . "/app/publisher/" . $item->id_publisher . "/" . $item->foto_publisher;
            $thumbnail = URL::to('/') . "/app/publisher/" . $item->id_publisher . "/" . $item->thum_publisher;

            $item->foto_publisher = !$item->foto_publisher ? $this->default_img : $image;
            $item->thum_publisher = !$item->thum_publisher ? $this->default_thumb : $thumbnail;

            return $item;
        });

        return $publishers;
    }

    /**
     * Return a publisher
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $publisher = Publisher::findOrFail($id);

            $image = URL::to('/') . "/app/publisher/" . $publisher->id_publisher . "/" . $publisher->foto_publisher;
            $thumbnail = URL::to('/') . "/app/publisher/" . $publisher->id_publisher . "/" . $publisher->thum_publisher;

            $publisher->foto_publisher = !$publisher->foto_publisher ? $this->default_img : $image;
            $publisher->thum_publisher = !$publisher->thum_publisher ? $this->default_thumb : $thumbnail;

            return $publisher;
        } catch (ModelNotFoundException $e) {
            throw new PublisherNotFoundException;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Return all publisher teams
     *
     * @return void
     */
    public function getTeams()
    {
        try {
            $teams = Publisher_team::get();

            return $teams;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Return all publisher teams
     *
     * @param int $id
     * @return void
     */
    public function findTeams($id)
    {
        try {
            $publisher = $this->find($id);
            $teams = Publisher_team::where('id_publisher', $publisher->id_publisher)->get();

            return $teams;
        } catch (PublisherNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
