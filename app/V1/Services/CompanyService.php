<?php

namespace App\V1\Services;

use App\Models\Company;
use App\V1\Contracts\CompanyServiceContract;
use App\V1\Exceptions\CompanyNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class CompanyService implements CompanyServiceContract
{
    /**
     * Returns all companies.
     *
     * @return Collection
     */
    public function get()
    {
        return Company::get();
    }

    /**
     * Returns company by ID.
     *
     * @param $id
     * @return Company
     */
    public function find($id)
    {
        try {
            return Company::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CompanyNotFoundException;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
