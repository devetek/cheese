<?php

namespace App\V1\Services;

use App\User;

class RegistrationService
{
    public function register(array $data)
    {
        try {
            return User::create($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
