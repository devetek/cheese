<?php

namespace App\V1\Services;

use App\Exceptions\TicketNotFoundException;
use App\Models\Ticket;
use App\Models\Ticket_answer;
use App\V1\Contracts\TicketServiceContract;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TicketService implements TicketServiceContract
{
    /**
     * Get all tickets based on input
     *
     * @param int $sub_module_id
     * @return void
     */
    public function get($sub_module_id)
    {
        try {
            $tickets = Ticket::where('sub_module_id', $sub_module_id)->get();

            $mapped_tickets = $tickets->map(function ($item) {
                $user = User::find($item->id_user);
                $item->name = ! is_null($user) ? $user->name : '';

                return $item;
            });

            return $mapped_tickets;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Find a ticket
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return Ticket::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new TicketNotFoundException;
        }
    }

    /**
     * Create new ticket
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $ticket = Ticket::create($data);

            return $ticket;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Find answers of ticket
     *
     * @param int $id
     * @return void
     */
    public function getAnswers($id)
    {
        try {
            $ticket = $this->find($id);
            $answers = Ticket_answer::where('id_ticket', $ticket->id_ticket)->get();

            $mapped_answers = $answers->map(function ($item) {
                $user = User::find($item->id_user);
                $item->name = ! is_null($user) ? $user->name : '';

                return $item;
            });

            return $mapped_answers;
        } catch (TicketNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
