<?php

namespace App\V1\Services;

use App\V1\Contracts\ImageServiceContract;
use Illuminate\Support\Str;

class ImageService implements ImageServiceContract
{
    /**
     * Move image into given path
     *
     * @param string $path
     * @param string $file
     * @return void
     */
    public function upload($path, $file)
    {
        try {
            if (!file_exists($path)) {
                mkdir($path);
            }

            $image = $file;
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = Str::random(34).'.'.'jpeg';

        file_put_contents($path.'/'.$imageName, base64_decode($image));

        // Storage::disk('local')->put($path.$imageName,base64_decode($image));

        //     $file->move($path, $file->getClientOriginalName());

            return $imageName;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
