<?php

namespace App\V1\Services;

use App\User;
use App\V1\Contracts\UserServiceContract;
use App\V1\Exceptions\UserNotFoundException;
use App\V1\Exceptions\OldPasswordNotMatchException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class UserService implements UserServiceContract
{
    /**
     * Find single user by ID.
     *
     * @param int $id
     * @return Model
     */
    public function find($id)
    {
        try {
            return User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new UserNotFoundException;
        }
    }

    /**
     * Update an user.
     *
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update($id, array $data)
    {
        try {
            $user = $this->find($id);
            $user->update($data);

            return $user->refresh();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Change password of a user.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function changePassword($id, $data)
    {
        try {
            $user = $this->find($id);

            if (! password_verify($data['current_password'], $user->password)) {
                throw new OldPasswordNotMatchException;
            }

            $user->update([
                'password' => Hash::make($data['password']),
            ]);

            return $user;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
