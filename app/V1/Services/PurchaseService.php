<?php

namespace App\V1\Services;

use App\Models\Purchase;
use App\Models\V_packet_sale;
use App\Models\Purchase_confirm;
use App\V1\Contracts\PurchaseServiceContract;
use App\V1\Exceptions\PurchaseNotFoundException;
use App\V1\Exceptions\UnicodeFailException;
use App\V1\Exceptions\LessPaymentException;
use App\V1\Events\ReturningBackUniqueCode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class PurchaseService implements PurchaseServiceContract
{
    /**
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var ImageService
     */
    protected $image_service;

    /**
     * @var string
     */
    protected $image_path;

    /**
     * @var string
     */
    protected $package_cover;

    /**
     * @var string
     */
    protected $package_thumbnail;

    public function __construct(Client $client, ImageService $image_service)
    {
        $this->client = $client;
        $this->image_service = $image_service;
        $this->image_path = public_path() . '/confirm/';
        $this->package_cover = URL::to('/') . '/app/packet/default.png';
        $this->package_thumbnail = URL::to('/') . '/app/packet/thum_default.png';
    }

    /**
     * Find single purchase
     *
     * @param int $id
     * @return Purchase
     */
    public function find($id)
    {
        try {
            return Purchase::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new PurchaseNotFoundException($id);
        }
    }

    /**
     * Add purchase data and process to checkout
     *
     * @return void
     */
    public function create(array $data)
    {
        try {
            $package = V_packet_sale::findOrFail($data['sale_package_id']);
            $user_id = Auth::user()->id;

            $purchase = new Purchase;
            $purchase->id_user = $user_id;
            $purchase->validasi = 1;
            $purchase->module_id = $data['sale_package_id'];
            $purchase->for_module = 'packet';
            $purchase->price_purchase = $package->price_packet_sale;
            $purchase->id_member = Auth::user()->id_from_division;
            $purchase->unique_code = $this->getUniqueCode();

            $purchase->save();

            return $purchase;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Confirming purchase
     *
     * @param array $data
     * @return void
     */
    public function confirm($id, array $data)
    {
        try {
            $purchase = $this->find($id);

            if ($data['amount_paid'] < $purchase->amount_paid) {
                throw new LessPaymentException;
            }

            $this->image_path = $this->image_path . $id . '/';
            $file_name = $this->image_service->upload($this->image_path, $data['file']);
            $purchase->amount_paid = $purchase->price + $purchase->unique_code;

            $purchase_confirm = new Purchase_confirm;
            $purchase_confirm->transfer_form_chanel = $data['transferred_form_channel'];
            $purchase_confirm->transfer_form_name = $data['transferred_form_name'];
            $purchase_confirm->value_paid = $data['amount_paid'];
            $purchase_confirm->id_purchase = $id;
            $purchase_confirm->date_confirm = date('Y-m-d');
            $purchase_confirm->foto_confirm = $file_name;
            $purchase_confirm->validasi = 1;
            $purchase_confirm->id_user = Auth::user()->id;

            $purchase->save();
            $purchase_confirm->save();
            event(new ReturningBackUniqueCode((int) $purchase->unique_code));

            return $purchase->refresh();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Return an unique code.
     *
     * @return void
     */
    public function getUniqueCode()
    {
        try {
            $response = $this->client->request('GET', 'v2/payment/unicode');
            $encoded = json_decode((string) $response->getBody(), true);

            return (int) $encoded['code'];
        } catch (RequestException $e) {
            throw new UnicodeFailException;
        } catch (\Exception $e) {
            event(new ReturningBackUniqueCode((int) $encoded['code']));
            throw $e;
        }
    }

    /**
     * Return all purchases by a user.
     *
     * @param int $user_id
     * @return void
     */
    public function getUnpaidPurchase($user_id)
    {
        try {
            $data = DB::table('purchase')
                ->join(
                    'v_packet_sale_all',
                    'purchase.module_id',
                    '=',
                    'v_packet_sale_all.id_packet_sale'
                )
                ->leftjoin(
                    'purchase_confirm',
                    'purchase.id_purchase',
                    '=',
                    'purchase_confirm.id_purchase'
                )
                ->select(
                    'purchase.id_purchase',
                    'purchase_confirm.value_paid as amount_paid',
                    'purchase.date_purchase',
                    'purchase.unique_code',
                    'purchase.price_purchase',
                    'v_packet_sale_all.*'
                )
                ->where([
                    'purchase.id_user_accept' => null,
                    'purchase.date_accept' => null,
                    'purchase.id_user' => $user_id,
                ])->get();

            $mapped_purchase = collect($data)->map(function ($item) {
                $cover = URL::to('/') . "/app/packet/" . $item->id_packet . "/" . $item->cover_packet;
                $thumbnail = URL::to('/') . "/app/packet/" . $item->id_packet . "/" . $item->thum_packet;

                $item->cover_packet = !$item->cover_packet ? $this->package_cover : $cover;
                $item->thum_packet = !$item->thum_packet ? $this->package_thumbnail : $thumbnail;
                $item->total = $item->price_purchase + $item->unique_code;

                return $item;
            });

            return $mapped_purchase;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {
        try {
            $purchase = $this->find($id);
            $purchase->delete();
        } catch (ModelNotFoundException $e) {
            throw new PurchaseNotFoundException;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
