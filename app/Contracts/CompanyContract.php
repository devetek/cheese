<?php

namespace App\Contracts;

interface CompanyContract
{
    public function get();
    public function find($id);
}
