<?php

namespace App\Repositories\Member\admin;

use App\Helper\Query;
use App\Models\Purchase_packet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class Purchase_packetRepository
 * @package App\Repositories\Member\admin
 * @version March 10, 2019, 3:44 am UTC
 *
 * @method Purchase_packet find($id, $columns = ['*'])
 * @method Purchase_packet first($columns = ['*'])
*/
class Purchase_packetRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'for_module',
        'unique_code',
        'module_id',
        'price_purchase',
        'date_purchase',
        'id_member',
        'id_user',
        'qty',
        'id_user_accept',
        'date_accept',
        'amount_paid',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase_packet::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "SELECT
                    a.*,
                    c.*,
                    b.id_packet_sale
                FROM purchase a,
                    packet_sale b,
                    packet c
                WHERE a.module_id=b.id_packet_sale
                and b.id_packet=c.id_packet
                and a.for_module='packet_sale'
                and a.validasi > 0 ".$query."
                order by a.created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];

        foreach ($data as $x) {
            if ($x->cover_packet == null) {
                $x->cover_packet = URL::to('/') . "/app/packet/default.png";
            } else {
                $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;

            }
            if ($x->thum_packet == null) {
                $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
            } else {
                $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

}
