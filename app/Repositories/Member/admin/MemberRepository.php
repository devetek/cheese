<?php

namespace App\Repositories\Member\admin;

use App\Helper\Query;
use App\Models\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class MemberRepository
 * @package App\Repositories\Member\admin
 * @version March 10, 2019, 1:48 am UTC
 *
 * @method Member find($id, $columns = ['*'])
 * @method Member first($columns = ['*'])
 */
class MemberRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_company',
        'first_name',
        'last_name',
        'last_education',
        'phone_member',
        'poscode_member',
        'email_member',
        'foto_member',
        'thum_member',
        'address_member',
        'status_member',
        'register_form',
        'validasi',
        'id_user_rgister'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Member::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "Select * from member

                where validasi > 0 " . $query . "
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function detail($id)
    {
        $member = Member::find($id);
        if($member->foto_member == null)
        {
            $member->foto_member = URL::to('/')."/app/member/default.png";
        }
        else
        {
            $member->foto_member = URL::to('/')."/app/member/".$member->id_member."/".$member->foto_member;
        }

        if($member->thum_member == null)
        {
            $member->thum_member = URL::to('/')."/app/member/thum_default.png";
        }
        else
        {
            $member->thum_member = URL::to('/')."/app/member/".$member->id_member."/".$member->thum_member;

        }

        return $member;
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Member($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Member::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
