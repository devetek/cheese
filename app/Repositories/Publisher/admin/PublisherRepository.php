<?php

namespace App\Repositories\Publisher\admin;

use App\Helper\Query;
use App\Models\Publisher;
use App\Models\v_publisher;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class PublisherRepository
 * @package App\Repositories\Publisher\admin
 * @version February 15, 2019, 4:36 am UTC
 *
 * @method Publisher find($id, $columns = ['*'])
 * @method Publisher first($columns = ['*'])
*/
class PublisherRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'public_name',
        'title_publisher',
        'foto_publisher',
        'thum_publisher',
        'phone_number',
        'mail_publisher',
        'id_company',
        'join_date',
        'status_publisher',
        'validasi'
    ];

    /**
     * @var string
     */
    private $default_img;

    /**
     * @var string
     */
    private $default_thumb;

    public function __construct()
    {
        $this->default_img = URL::to('/') . "/app/publisher/thum_default.png";
        $this->default_thumb = URL::to('/') . "/app/publisher/thum_default.png";
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Publisher::class;
    }

    public function all()
    {
        $sql = "Select * from v_publisher
                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        
        foreach ($data as $x) {
            $image = URL::to('/') . "/app/publisher/" . $x->id_publisher . "/" . $x->foto_publisher;
            $thumbnail = URL::to('/') . "/app/publisher/" . $x->id_publisher . "/" . $x->thum_publisher;

            $x->foto_publisher = !$x->foto_publisher ? $this->default_img : $image;
            $x->thum_publisher = !$x->thum_publisher ? $this->default_thumb : $thumbnail;
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function detail($id, $columns = ['*'])
    {
        $publisher = v_publisher::find($id);

        $image = URL::to('/') . "/app/publisher/" . $publisher->id_publisher . "/" . $publisher->foto_publisher;
        $thumbnail = URL::to('/') . "/app/publisher/" . $publisher->id_publisher . "/" . $publisher->thum_publisher;

        $publisher->foto_publisher = !$publisher->foto_publisher ? $this->default_img : $image;
        $publisher->thum_publisher = !$publisher->thum_publisher ? $this->default_thumb : $thumbnail;

        return $publisher;
    }

    public function create(array $attributes)
    {
        $model = new Publisher($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Publisher::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }
}
