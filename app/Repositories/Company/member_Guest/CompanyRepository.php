<?php

namespace App\Repositories\Company\member_Guest;

use App\Helper\Query;
use App\Models\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class CompanyRepository
 * @package App\Repositories\Company\member_Guest
 * @version February 19, 2019, 3:45 pm UTC
 *
 * @method Company find($id, $columns = ['*'])
 * @method Company first($columns = ['*'])
*/
class CompanyRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_company',
        'logo_company',
        'thum_company',
        'address_company',
        'about_company',
        'link_company',
        'id_head_company',
        'phone_company',
        'status_company',
        'id_user',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Company::class;
    }

    public function all()
    {
        $sql = "select a.*
                    FROM company a,
                        company_category b
                    WHERE a.id_company=b.id_company
                    AND b.id_company_master_category in (1,2)
                    GROUP BY a.id_company
                ";
        $data = DB::select($sql);

        $out = [];

        foreach ($data as $x)
        {
            if($x->logo_company == null)
            {
                $x->logo_company = URL::to('/')."/app/company/default.png";
                $x->thum_company = URL::to('/')."/app/company/thum_default.png";
            }
            else
            {
                $x->logo_company = URL::to('/')."/app/company/".$x->id_packet."/".$x->logo_company;
                $x->thum_company = URL::to('/')."/app/company/".$x->id_packet."/".$x->thum_company;

            }
            $out[] = $x;
        }

        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Company($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Company::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
