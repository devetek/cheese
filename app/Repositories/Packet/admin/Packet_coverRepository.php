<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet;
use Illuminate\Support\Facades\DB;

/**
 * Class PacketRepository
 * @package App\Repositories\Packet\admin
 * @version March 3, 2019, 12:58 pm UTC
 *
 * @method Packet find($id, $columns = ['*'])
 * @method Packet first($columns = ['*'])
 */
class Packet_coverRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'id_user',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet::class;
    }


    public function create($request,array $attributes)
    {
        $file = $request->file('file');
        if ($file != null) {
            $request->request->add(['file' => $file->getClientOriginalName()]);

            $pacth = public_path() . '/app/packet/' . $request->input('id') . "/";
            if (!file_exists($pacth)) {
                mkdir($pacth, 0777, true);
            }
            $file->move($pacth, $file->getClientOriginalName());

            $book = Packet::find($request->input('id'));
            if ($request->input('type') == 'cover') {
                $book->cover_packet = $request->input('file');

            } else {
                $book->thum_packet = $request->input('file');
            }

            $book->save();

            return $book;
        }
        return "";
    }


}
