<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_goal;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_goalRepository
 * @package App\Repositories\Packet\admin
 * @version April 1, 2019, 1:12 am UTC
 *
 * @method Packet_goal find($id, $columns = ['*'])
 * @method Packet_goal first($columns = ['*'])
*/
class Packet_goalRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'order_goal',
        'goal',
        'icon'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_goal::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from packet_goal

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet_goal($attributes);
            $model->save();
            return $model;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_goal::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
