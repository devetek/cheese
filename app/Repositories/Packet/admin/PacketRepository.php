<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class PacketRepository
 * @package App\Repositories\Packet\admin
 * @version March 3, 2019, 4:58 am UTC
 *
 * @method Packet find($id, $columns = ['*'])
 * @method Packet first($columns = ['*'])
*/
class PacketRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select 
                a.*,
                b.name_company 
                from packet a,
                company b 
                where a.id_company=b.id_company 
                and a.validasi > 0 ".$query."
                order by a.created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function detail($id, $columns = ['*'])
    {
        $data = Packet::find($id);

        if ($data->cover_packet == null) {
            $data->cover_packet = URL::to('/') . "/app/packet/default.png";
        } else {
            $data->cover_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->cover_packet;

        }
        if ($data->thum_packet == null) {
            $data->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
        } else {
            $data->thum_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->thum_packet;

        }
        $out['packet'] = $data;
        /*------------------------Widget ------------------------------*/
        //1. Aktif
        $sql="Select count(*) as total from v_my_packet_support where id_packet='".$id."' and status_support > 0";
        $out['active'] = DB::select($sql)[0];
        //2. Total Terjual
        $sql="Select count(*) as total from v_my_packet where id_packet=$id";
        $out['total'] = DB::select($sql)[0];
        //3. Total Tiket
        $sql="Select count(*) as total from ticket where module_id=$id";
        $out['ticket'] = DB::select($sql)[0];

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
