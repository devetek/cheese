<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\MyPacket;
use Illuminate\Support\Facades\DB;

/**
 * Class MyPacketRepository
 * @package App\Repositories\Packet\admin
 * @version April 7, 2019, 11:41 am UTC
 *
 * @method MyPacket find($id, $columns = ['*'])
 * @method MyPacket first($columns = ['*'])
*/
class PacketIncomeRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'validasi',
        'id_packet_sale',
        'promo_text',
        'price_packet_sale',
        'time_support',
        'phone_company',
        'name_company',
        'about_company',
        'id_member_subscribe',
        'amount_paid',
        'date_active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MyPacket::class;
    }

    public function all($request)
    {
        $tahun = date('Y');
        if($request->input('tahun'))
        {
            if($request->input('tahun')!=null)
            {
                $tahun = $request->input('tahun');
            }
        }
        $out['tahun'] = $tahun;

        $query = "";
        for ($a=1; $a <= 12; $a++)
        {
            $query.=" sum(if(month(a.date_accept) =$a and year(a.date_accept)=$tahun,1,0)) as total_jual_".$a.",
                    sum(if(month(a.date_accept) =$a and year(a.date_accept)=$tahun,a.price_purchase,0)) as total_harga_".$a.", ";
        }
        $sql = "SELECT
                    ".$query."
                    count(*) as total,
                    sum(a.price_purchase) as total_price
                FROM purchase a,
                    packet_sale b
                WHERE a.module_id=b.id_packet_sale
                    AND a.for_module='packet'
                    and b.id_packet =  ".$request->input('id_packet')." 
                    and a.amount_paid > 0";
        $out['data'] = DB::select($sql);
        return $out;
    }



}
