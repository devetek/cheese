<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_sale;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_saleRepository
 * @package App\Repositories\Packet\admin
 * @version March 16, 2019, 12:38 am UTC
 *
 * @method Packet_sale find($id, $columns = ['*'])
 * @method Packet_sale first($columns = ['*'])
 */
class Packet_saleRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promo_text',
        'id_packet',
        'efective_date',
        'id_user',
        'is_priority',
        'time_support',
        'price_packet_sale',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_sale::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "Select * from packet_sale

                where validasi > 0 " . $query . "
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Packet_sale($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $sql = "Update packet_sale set status_packet_sale='inactive' where id_packet='" . $attributes['id_packet'] . "'";
        DB::update($sql);

        $data = Packet_sale::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
