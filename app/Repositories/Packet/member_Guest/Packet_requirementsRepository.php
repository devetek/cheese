<?php

namespace App\Repositories\Packet\member_Guest;

use App\Helper\Query;
use App\Models\Packet_requirements;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_requirementsRepository
 * @package App\Repositories\Packet\member_Guest
 * @version February 21, 2019, 7:10 am UTC
 *
 * @method Packet_requirements find($id, $columns = ['*'])
 * @method Packet_requirements first($columns = ['*'])
*/
class Packet_requirementsRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_paket',
        'order_requirement',
        'requirement'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_requirements::class;
    }

    public function all($request)
    {
        $sql = "Select * from packet_requirements 
                where id_packet = ".$request->input('id_packet')."
                order by order_requirement DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet_requirements($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_requirements::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
