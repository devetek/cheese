<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\Packet_goal;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_goalRepository
 * @package App\Repositories\Packet\client
 * @version April 7, 2019, 3:43 pm UTC
 *
 * @method Packet_goal find($id, $columns = ['*'])
 * @method Packet_goal first($columns = ['*'])
*/
class Packet_goalRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'order_goal',
        'goal',
        'icon',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_goal::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from packet_goal

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

}
