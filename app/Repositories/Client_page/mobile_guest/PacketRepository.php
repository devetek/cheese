<?php

namespace App\Repositories\Client_page\mobile_guest;

use App\Helper\Query;
use App\Models\Packet_detail;
use App\Models\Slider_img;
use App\Models\V_packet_sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class PacketRepository
{


    public function detail($id_packet)
    {
        $data['packet'] = $this->getPacket($id_packet);
        $data['packet_detail'] = $this->getPacketDetail($id_packet);
        $data['packet_goal'] = $this->getPacketGoal($id_packet);
        return $data;
    }

    public function getPacket($id_packet)
    {
        $data = V_packet_sale::where('id_packet',$id_packet)->get();
        $data = $data[0];
        if ($data->cover_packet == null) {
            $data->cover_packet = URL::to('/') . "/app/packet/default.png";
        } else {
            $data->cover_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->cover_packet;

        }

        if ($data->thum_packet == null) {
            $data->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
        } else {
            $data->thum_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->thum_packet;
        }

        return $data;
    }

    public function getPacketDetail($id_packet)
    {
        $sql = "Select * from packet_detail

                where id_packet = " . $id_packet . "
                order by order_detail ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function getPacketGoal($id_packet)
    {
        $sql = "Select * from packet_goal

                where id_packet = " . $id_packet . "
                order by order_goal ASC
                ";
        $data = DB::select($sql);
        return $data;
    }



}
