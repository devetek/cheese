<?php

namespace App\Repositories\Client_Page\web;

use App\Helper\Query;
use App\Models\Member;
use App\Models\Purchase;
use App\Models\V_packet_sale;
use App\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

/**
 * Class PurchaseRepository
 * @package App\Repositories\Client_Page\web
 * @version April 1, 2019, 2:26 pm UTC
 *
 * @method Purchase find($id, $columns = ['*'])
 * @method Purchase first($columns = ['*'])
 */
class PurchaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'for_module',
        'unique_code',
        'module_id',
        'price_purchase',
        'date_purchase',
        'id_member',
        'id_user',
        'qty',
        'id_user_accept',
        'date_accept',
        'amount_paid',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "SELECT
                    a.id_purchase,
                    a.amount_paid,
                    a.date_purchase,
                    b.*,
                    x.date_confirm
                FROM purchase a
                    LEFT JOIN purchase_confirm x on a.id_purchase=x.id_purchase,
                    v_packet_sale b
                WHERE a.module_id = b.id_packet_sale
                      AND a.amount_paid IS NULL
                and a.id_user=".Query::getUser()->id."
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x) {
            if ($x->cover_packet == null) {
                $x->cover_packet = URL::to('/') . "/app/packet/default.png";
            } else {
                $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;
            }
            if ($x->thum_packet == null) {
                $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
            } else {
                $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;
            }
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Purchase($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update($request, $id)
    {
        $packet_sale =  V_packet_sale::find($request->input('id_packet_sale'));
        $data = Purchase::find($id);
        $data->unique_code = 123;
        $data->module_id = $request->input('id_packet_sale');
        $data->for_module = 'packet';
        $data->validasi = 1;
        $data->price_purchase = $packet_sale->price_packet_sale;
        $data->id_member = Query::getUser()->id_from_division;
        $data->save();
        return $data;
    }

    public function pree($request)
    {
        $data['packet'] = V_packet_sale::find($request->input('id_packet_sale'));
        $data['user'] = Query::getUser();
        $data['member'] = Member::find($data['user']->id_from_division);
        return $data;
    }
}
