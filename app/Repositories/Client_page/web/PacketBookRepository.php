<?php

namespace App\Repositories\Client_page\web;

use App\Helper\Query;
use App\Models\Packet_detail;
use App\Models\Slider_img;
use App\Models\V_packet_sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class PacketBookRepository
{


    public function all($id_packet)
    {
        $data = $this->getPacketBook($id_packet);
        return $data;
    }


    public function getPacketBook($request)
    {
        $id_packet = $request->input('id_packet');
        $sql = "Select 
                    b.*,
                    a.id_packet_book
                from packet_book a,
                v_book b 
                where a.id_book=b.id_book 
                and a.validasi > 0 
                and a.id_packet='" . $id_packet . "'
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x) {
            if ($x->cover_url == null) {
                $x->cover_url = URL::to('/') . "/app/book/default.png";
            } else {
                $x->cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->cover_url;

            }

            if ($x->thum_cover_url == null) {
                $x->thum_cover_url = URL::to('/') . "/app/book/thum_default.png";
            } else {
                $x->thum_cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out;
    }


    public function detail($id_book)
    {
        $data['book'] = self::detailBook($id_book);
        $data['section'] = self::detailBookSection($id_book);

        return $data;
    }

    public function detailBook($id_book)
    {
        $sql = "Select 
                    b.*,
                    a.id_packet_book
                from packet_book a,
                v_book b 
                where a.id_book=b.id_book 
                and a.validasi > 0 
                and a.id_book='" . $id_book . "'
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x) {
            if ($x->cover_url == null) {
                $x->cover_url = URL::to('/') . "/app/book/default.png";
            } else {
                $x->cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->cover_url;

            }

            if ($x->thum_cover_url == null) {
                $x->thum_cover_url = URL::to('/') . "/app/book/thum_default.png";
            } else {
                $x->thum_cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out[0];
    }

    public function detailBookSection($id_book)
    {
        $sql = " select *
                from book_section a 
                where a.id_book = '".$id_book."'
                and a.validasi > 0
                order by order_book_section ASC
                ";
        $data = DB::select($sql);

        $out = [];

        foreach ($data as $x)
        {
            $sql="Select 
                    a.order_book_content,
                    a.time_book_content,
                    a.title_book_content
                    from book_content a 
                    where a.id_book_section='".$x->id_book_section."'
                    and a.validasi > 0
                    order by a.order_book_content ASC ";
            $x->content = DB::select($sql);
            $out[] = $x;
        }

        return $out;

    }


}
