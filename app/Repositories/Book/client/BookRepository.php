<?php

namespace App\Repositories\Book\client;

use App\Helper\Query;
use App\Models\Book;
use App\Models\V_book;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class BookRepository
 * @package App\Repositories\Book\client
 * @version April 8, 2019, 12:02 am UTC
 *
 * @method Book find($id, $columns = ['*'])
 * @method Book first($columns = ['*'])
 */
class BookRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_book',
        'org_price',
        'actual_price',
        'description',
        'promotional_text',
        'id_publisher',
        'id_team',
        'id_user',
        'cover_url',
        'thum_cover_url',
        'date_publish',
        'status',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Book::class;
    }


    public function detail($id)
    {
        $data['book'] = self::detailBook($id);
        $data['sections'] = self::detailBookSection($id);
        return $data;
    }

    public function detailBook($id)
    {
        $sql = "Select 
                    b.*,
                    a.id_packet_book,
                    a.id_packet
                from packet_book a,
                v_book b 
                where a.id_book=b.id_book 
                and a.validasi > 0 
                and a.id_book='" . $id . "'
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x) {
            if ($x->cover_url == null) {
                $x->cover_url = URL::to('/') . "/app/book/default.png";
            } else {
                $x->cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->cover_url;

            }

            if ($x->thum_cover_url == null) {
                $x->thum_cover_url = URL::to('/') . "/app/book/thum_default.png";
            } else {
                $x->thum_cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out[0];
    }

    public function detailBookSection($id_book)
    {
        $sql = " select *
                from book_section a 
                where a.id_book = '".$id_book."'
                and a.validasi > 0
                order by order_book_section ASC
                ";
        $data = DB::select($sql);

        $out = [];

        foreach ($data as $x)
        {
            $sql="Select 
                    a.order_book_content,
                    a.time_book_content,
                    a.id_book_content,
                    a.title_book_content
                    from book_content a 
                    where a.id_book_section='".$x->id_book_section."'
                    and a.validasi > 0
                    order by a.order_book_content ASC ";
            $x->content = DB::select($sql);
            $out[] = $x;
        }

        return $out;

    }


}
