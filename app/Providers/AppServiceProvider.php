<?php

namespace App\Providers;

use App\Libraries\CustomUrlGenerator;
use App\V1\Services\PurchaseService;
use App\V1\Services\ImageService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Register services
         */
        $this->app->singleton(Client::class, function () {
            return new Client([
                'base_uri' => env('HOMPES_DEV_URI') ?? 'http://apidev.hompes.id/',
            ]);
        });

        $this->app->bind(PurchaseService::class, function () {
            return new PurchaseService(app(Client::class), app(ImageService::class));
        });

        /**
         * Register custom url generator, make flexible base url base on APP_URL environment variable.
         * Author: Prakasa <prakasa@devetek.com>
         */
        $routes = $this->app['router']->getRoutes();
        $customUrlGenerator = new CustomUrlGenerator($routes, $this->app->make('request'));
        $this->app->instance('url', $customUrlGenerator);
    }
}
