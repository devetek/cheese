<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
        $this->packetRouter();
        $this->bookRouter();
        $this->userRouter();
        $this->publisherRouter();
        $this->companyRouter();
        $this->slideApiRoutes();
        $this->memberRouter();
        $this->purchaseRouter();
        $this->ClientRouter();
        $this->ticketRouter();

        $this->v1CompanyRoutes();
        $this->v1PublisherRoutes();
        $this->v1PurchaseRoutes();
        $this->v1MyPurchaseRoutes();
        $this->v1TicketRoutes();
        $this->v1UserRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Map V1 company routes.
     *
     * @return void
     */
    protected function v1CompanyRoutes()
    {
        Route::prefix('api/v1/companies')
            ->group(base_path('routes/v1/company.php'));
    }

    /**
     * Map V1 publisher routes.
     *
     * @return void
     */
    protected function v1PublisherRoutes()
    {
        Route::prefix('api/v1/publishers')
            ->group(base_path('routes/v1/publisher.php'));
    }

    /**
     * Map V1 purchase routes
     *
     * @return void
     */
    protected function v1PurchaseRoutes()
    {
        Route::prefix('api/v1/purchases')
            ->group(base_path('routes/v1/purchase.php'));
    }

    /**
     * Map V1 User routes
     *
     * @return void
     */
    protected function v1UserRoutes()
    {
        Route::prefix('api/v1/users')
            ->group(base_path('routes/v1/user.php'));
    }

    /**
     * Map V1 my purchase routes
     *
     * @return void
     */
    protected function v1MyPurchaseRoutes()
    {
        Route::prefix('/api/v1/my-purchases')
            ->group(base_path('routes/v1/my_purchase.php'));
    }

    /**
     * Map V1 tickets routes
     *
     * @return void
     */
    protected function v1TicketRoutes()
    {
        Route::prefix('/api/v1/tickets')
            ->group(base_path('routes/v1/ticket.php'));
    }

    protected function slideApiRoutes()
    {
        Route::prefix('api/v1/slider')
             ->middleware('auth:api')
             ->namespace($this->namespace."\\API\\Slider")
             ->group(base_path('routes/slide.php'));
    }

    protected function ticketRouter()
    {
        Route::prefix('api/v1/ticket')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Ticket")
            ->group(base_path('routes/ticket.php'));
    }
    protected function bookRouter()
    {
        Route::prefix('api/v1/book')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Book")
            ->group(base_path('routes/book.php'));
    }
    protected function purchaseRouter()
    {
        Route::prefix('api/v1/purchase')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Purchase")
            ->group(base_path('routes/purchase.php'));
    }

    protected function packetRouter()
    {
        Route::prefix('api/v1/packet')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Packet")
            ->group(base_path('routes/packet.php'));
    }

    protected function userRouter()
    {
        Route::prefix('api/v1/user')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\User")
            ->group(base_path('routes/user.php'));
    }

    protected function publisherRouter()
    {
        Route::prefix('api/v1/publisher')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Publisher")
            ->group(base_path('routes/publisher.php'));
    }

    protected function companyRouter()
    {
        Route::prefix('api/v1/company')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Company")
            ->group(base_path('routes/company.php'));
    }


    protected function memberRouter()
    {
        Route::prefix('api/v1/member')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Member")
            ->group(base_path('routes/member.php'));
    }

    protected function ClientRouter()
    {
        Route::prefix('api/v1/client')
            ->middleware('auth:api')
            ->as('api.')
            ->namespace($this->namespace."\\API\\Client_page")
            ->group(base_path('routes/client_page.php'));
    }
}
