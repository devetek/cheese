<?php

namespace App\Helper;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class Query
{
    public static function filterRequest($model, $request)
    {
        $sql = " ";
        $req = $request->input();
        foreach ($req as $key => $x) {
            if (in_array(strtolower($key), $model)) {
                $sql .= " and " . self::parameter($key, $x) . " ";
            }
        }

        return $sql;
    }

    public static function parameter($key, $param)
    {
        if (strpos($param, '%') !== false) {
            return " lower(" . $key . ") " . " like '" . (str_replace('%', '', $param)) . "%' ";
        }

        return $key . " = '" . $param . "'";
    }


    public static function getUser()
    {
        return Auth::user();

        $user = (object) [];
        $user->id = 1;
        $user->username = 'arivin29';
        return $user;
    }

    public static function getCompany()
    {
        $company = (object) [];
        $company->id_company = 1;
        $company->name_company = 'Hompes';
        return $company;
    }

    public static function beforeInsert($model, $request = null)
    {
        $sql = " Select * from " . $model->table . " where validasi < 1 and id_user=" . self::getUser()->id;

        if ($request != null) {
            foreach ($request->input() as $key => $x) {
                $sql .= " and " . self::parameter($key, $x) . " ";
            }
        }

        $result = DB::select($sql);

        if (count($result) < 1) {
            $model->id_user = self::getUser()->id;
            $model->validasi = 0;
            $model->save();

            $result = $result = DB::select($sql)[0];
        } else {
            $result = $result[0];
        }

        return $result;
    }

    public static function afterProsess($data)
    { }

    public static function kirimPesanJawab($title,$token)
    {

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('Hompes tanya Jawab');
        $notificationBuilder->setBody('Pertanyaan anda di jawab , '.$title)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        // $token = "eqTD6tH4ELU:APA91bHutxEQGgYM6gXbFzzuzICsefKIe73Rzr9qNxLc5pMEgWbqYJgBxm-gR-t0fXA--g65yrAjzRBiVjmG_WzDEgwqlTAAxKUtgmgQVHegbUJR-vJiH-UMxsyguINtoKUipvZU_U3H";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();

        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:error) - in production you should remove from your database the tokens
        $downstreamResponse->tokensWithError();
    }
}
