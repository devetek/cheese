<?php

$config = [
    'namespace' => 'App\V1\Controllers',
    'middleware' => 'api',
];

Route::group($config, function () {
    Route::get('/', 'CompanyController@index');
    Route::get('/{id}', 'CompanyController@show');
});
