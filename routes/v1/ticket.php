<?php

$config = [
    'namespace' => 'App\V1\Controllers',
    'middleware' => 'auth:api',
];

Route::group($config, function () {
    Route::post('/', 'TicketController@store');
    Route::get('/{id}/answers', 'TicketController@getAnswers');
    Route::get('/{submodule_id}', 'TicketController@get');
});
