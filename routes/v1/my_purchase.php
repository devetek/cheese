<?php

$config = [
    'namespace' => 'App\V1\Controllers',
    'middleware' => 'auth:api',
];

Route::group($config, function () {
    Route::get('/unpaid', 'MyPurchaseController@unpaid');
});
