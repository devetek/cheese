<?php

$config = [
    'namespace' => 'App\V1\Controllers',
    'middleware' => 'auth:api',
];

Route::group($config, function () {
    Route::post('/', 'PurchaseController@purchase');
    Route::post('/{id}/confirm-payment', 'PurchaseController@confirm');
    Route::delete('/{id}', 'PurchaseController@destroy');
});
