<?php

$config = [
    'namespace' => 'App\V1\Controllers',
    'middleware' => 'api',
];

Route::group($config, function () {
    Route::get('/', 'PublisherController@get');
    Route::get('/teams', 'PublisherController@getTeams');
    Route::get('/{id}', 'PublisherController@find');
    Route::get('/{id}/teams', 'PublisherController@findTeams');
});
