<?php

$config = [
    'namespace' => 'App\V1\Controllers',
    'middleware' => 'auth:api',
];

Route::group($config, function () {
    Route::get('/{id}', 'UserController@find');
    Route::patch('/{id}', 'UserController@update');
    Route::patch('/{id}/change-password', 'UserController@changePassword');
});
