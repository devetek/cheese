<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookSectionApiTest extends TestCase
{
    use MakeBookSectionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBookSection()
    {
        $bookSection = $this->fakeBookSectionData();
        $this->json('POST', '/api/v1/bookSections', $bookSection);

        $this->assertApiResponse($bookSection);
    }

    /**
     * @test
     */
    public function testReadBookSection()
    {
        $bookSection = $this->makeBookSection();
        $this->json('GET', '/api/v1/bookSections/'.$bookSection->id_book_section);

        $this->assertApiResponse($bookSection->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBookSection()
    {
        $bookSection = $this->makeBookSection();
        $editedBookSection = $this->fakeBookSectionData();

        $this->json('PUT', '/api/v1/bookSections/'.$bookSection->id_book_section, $editedBookSection);

        $this->assertApiResponse($editedBookSection);
    }

    /**
     * @test
     */
    public function testDeleteBookSection()
    {
        $bookSection = $this->makeBookSection();
        $this->json('DELETE', '/api/v1/bookSections/'.$bookSection->id_book_section);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bookSections/'.$bookSection->id_book_section);

        $this->assertResponseStatus(404);
    }
}
