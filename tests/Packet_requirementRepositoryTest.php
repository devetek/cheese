<?php

use App\Models\Packet_requirement;
use App\Repositories\Packet\client\Packet_requirementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_requirementRepositoryTest extends TestCase
{
    use MakePacket_requirementTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_requirementRepository
     */
    protected $packetRequirementRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetRequirementRepo = App::make(Packet_requirementRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_requirement()
    {
        $packetRequirement = $this->fakePacket_requirementData();
        $createdPacket_requirement = $this->packetRequirementRepo->create($packetRequirement);
        $createdPacket_requirement = $createdPacket_requirement->toArray();
        $this->assertArrayHasKey('id', $createdPacket_requirement);
        $this->assertNotNull($createdPacket_requirement['id'], 'Created Packet_requirement must have id specified');
        $this->assertNotNull(Packet_requirement::find($createdPacket_requirement['id']), 'Packet_requirement with given id must be in DB');
        $this->assertModelData($packetRequirement, $createdPacket_requirement);
    }

    /**
     * @test read
     */
    public function testReadPacket_requirement()
    {
        $packetRequirement = $this->makePacket_requirement();
        $dbPacket_requirement = $this->packetRequirementRepo->find($packetRequirement->id_packet_requirements);
        $dbPacket_requirement = $dbPacket_requirement->toArray();
        $this->assertModelData($packetRequirement->toArray(), $dbPacket_requirement);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_requirement()
    {
        $packetRequirement = $this->makePacket_requirement();
        $fakePacket_requirement = $this->fakePacket_requirementData();
        $updatedPacket_requirement = $this->packetRequirementRepo->update($fakePacket_requirement, $packetRequirement->id_packet_requirements);
        $this->assertModelData($fakePacket_requirement, $updatedPacket_requirement->toArray());
        $dbPacket_requirement = $this->packetRequirementRepo->find($packetRequirement->id_packet_requirements);
        $this->assertModelData($fakePacket_requirement, $dbPacket_requirement->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_requirement()
    {
        $packetRequirement = $this->makePacket_requirement();
        $resp = $this->packetRequirementRepo->delete($packetRequirement->id_packet_requirements);
        $this->assertTrue($resp);
        $this->assertNull(Packet_requirement::find($packetRequirement->id_packet_requirements), 'Packet_requirement should not exist in DB');
    }
}
