<?php

use App\Models\Packet_book;
use App\Repositories\Packet\client\Packet_bookRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_bookRepositoryTest extends TestCase
{
    use MakePacket_bookTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_bookRepository
     */
    protected $packetBookRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetBookRepo = App::make(Packet_bookRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_book()
    {
        $packetBook = $this->fakePacket_bookData();
        $createdPacket_book = $this->packetBookRepo->create($packetBook);
        $createdPacket_book = $createdPacket_book->toArray();
        $this->assertArrayHasKey('id', $createdPacket_book);
        $this->assertNotNull($createdPacket_book['id'], 'Created Packet_book must have id specified');
        $this->assertNotNull(Packet_book::find($createdPacket_book['id']), 'Packet_book with given id must be in DB');
        $this->assertModelData($packetBook, $createdPacket_book);
    }

    /**
     * @test read
     */
    public function testReadPacket_book()
    {
        $packetBook = $this->makePacket_book();
        $dbPacket_book = $this->packetBookRepo->find($packetBook->id_packet_book);
        $dbPacket_book = $dbPacket_book->toArray();
        $this->assertModelData($packetBook->toArray(), $dbPacket_book);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_book()
    {
        $packetBook = $this->makePacket_book();
        $fakePacket_book = $this->fakePacket_bookData();
        $updatedPacket_book = $this->packetBookRepo->update($fakePacket_book, $packetBook->id_packet_book);
        $this->assertModelData($fakePacket_book, $updatedPacket_book->toArray());
        $dbPacket_book = $this->packetBookRepo->find($packetBook->id_packet_book);
        $this->assertModelData($fakePacket_book, $dbPacket_book->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_book()
    {
        $packetBook = $this->makePacket_book();
        $resp = $this->packetBookRepo->delete($packetBook->id_packet_book);
        $this->assertTrue($resp);
        $this->assertNull(Packet_book::find($packetBook->id_packet_book), 'Packet_book should not exist in DB');
    }
}
