<?php

use Faker\Factory as Faker;
use App\Models\BookSection;
use App\Repositories\Book\member\BookSectionRepository;

trait MakeBookSectionTrait
{
    /**
     * Create fake instance of BookSection and save it in database
     *
     * @param array $bookSectionFields
     * @return BookSection
     */
    public function makeBookSection($bookSectionFields = [])
    {
        /** @var BookSectionRepository $bookSectionRepo */
        $bookSectionRepo = App::make(BookSectionRepository::class);
        $theme = $this->fakeBookSectionData($bookSectionFields);
        return $bookSectionRepo->create($theme);
    }

    /**
     * Get fake instance of BookSection
     *
     * @param array $bookSectionFields
     * @return BookSection
     */
    public function fakeBookSection($bookSectionFields = [])
    {
        return new BookSection($this->fakeBookSectionData($bookSectionFields));
    }

    /**
     * Get fake data of BookSection
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookSectionData($bookSectionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_book' => $fake->randomDigitNotNull,
            'order_book_section' => $fake->randomDigitNotNull,
            'book_section' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bookSectionFields);
    }
}
