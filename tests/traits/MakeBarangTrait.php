<?php

use Faker\Factory as Faker;
use App\Models\Barang;
use App\Repositories\Etos\BarangRepository;

trait MakeBarangTrait
{
    /**
     * Create fake instance of Barang and save it in database
     *
     * @param array $barangFields
     * @return Barang
     */
    public function makeBarang($barangFields = [])
    {
        /** @var BarangRepository $barangRepo */
        $barangRepo = App::make(BarangRepository::class);
        $theme = $this->fakeBarangData($barangFields);
        return $barangRepo->create($theme);
    }

    /**
     * Get fake instance of Barang
     *
     * @param array $barangFields
     * @return Barang
     */
    public function fakeBarang($barangFields = [])
    {
        return new Barang($this->fakeBarangData($barangFields));
    }

    /**
     * Get fake data of Barang
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBarangData($barangFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama_barang' => $fake->word,
            'satuan' => $fake->word,
            'status_barang' => $fake->word,
            'kode_barang' => $fake->word,
            'barcode_barang' => $fake->word,
            'is_return' => $fake->randomDigitNotNull,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s')
        ], $barangFields);
    }
}
