<?php

use Faker\Factory as Faker;
use App\Models\Packet_cover;
use App\Repositories\Packet\admin\Packet_coverRepository;

trait MakePacket_coverTrait
{
    /**
     * Create fake instance of Packet_cover and save it in database
     *
     * @param array $packetCoverFields
     * @return Packet_cover
     */
    public function makePacket_cover($packetCoverFields = [])
    {
        /** @var Packet_coverRepository $packetCoverRepo */
        $packetCoverRepo = App::make(Packet_coverRepository::class);
        $theme = $this->fakePacket_coverData($packetCoverFields);
        return $packetCoverRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_cover
     *
     * @param array $packetCoverFields
     * @return Packet_cover
     */
    public function fakePacket_cover($packetCoverFields = [])
    {
        return new Packet_cover($this->fakePacket_coverData($packetCoverFields));
    }

    /**
     * Get fake data of Packet_cover
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_coverData($packetCoverFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name_packet' => $fake->word,
            'excerpt_packet' => $fake->text,
            'description_packet' => $fake->text,
            'id_publisher_team' => $fake->randomDigitNotNull,
            'cover_packet' => $fake->word,
            'thum_packet' => $fake->word,
            'date_publish' => $fake->word,
            'id_company' => $fake->randomDigitNotNull,
            'status_packet' => $fake->word,
            'is_public' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetCoverFields);
    }
}
