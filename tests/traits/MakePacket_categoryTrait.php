<?php

use Faker\Factory as Faker;
use App\Models\Packet_category;
use App\Repositories\Packet\client\Packet_categoryRepository;

trait MakePacket_categoryTrait
{
    /**
     * Create fake instance of Packet_category and save it in database
     *
     * @param array $packetCategoryFields
     * @return Packet_category
     */
    public function makePacket_category($packetCategoryFields = [])
    {
        /** @var Packet_categoryRepository $packetCategoryRepo */
        $packetCategoryRepo = App::make(Packet_categoryRepository::class);
        $theme = $this->fakePacket_categoryData($packetCategoryFields);
        return $packetCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_category
     *
     * @param array $packetCategoryFields
     * @return Packet_category
     */
    public function fakePacket_category($packetCategoryFields = [])
    {
        return new Packet_category($this->fakePacket_categoryData($packetCategoryFields));
    }

    /**
     * Get fake data of Packet_category
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_categoryData($packetCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_packet_master_category' => $fake->randomDigitNotNull,
            'id_packet' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetCategoryFields);
    }
}
