<?php

use Faker\Factory as Faker;
use App\Models\Packet_book;
use App\Repositories\Packet\client\Packet_bookRepository;

trait MakePacket_bookTrait
{
    /**
     * Create fake instance of Packet_book and save it in database
     *
     * @param array $packetBookFields
     * @return Packet_book
     */
    public function makePacket_book($packetBookFields = [])
    {
        /** @var Packet_bookRepository $packetBookRepo */
        $packetBookRepo = App::make(Packet_bookRepository::class);
        $theme = $this->fakePacket_bookData($packetBookFields);
        return $packetBookRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_book
     *
     * @param array $packetBookFields
     * @return Packet_book
     */
    public function fakePacket_book($packetBookFields = [])
    {
        return new Packet_book($this->fakePacket_bookData($packetBookFields));
    }

    /**
     * Get fake data of Packet_book
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_bookData($packetBookFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_book' => $fake->randomDigitNotNull,
            'id_packet' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetBookFields);
    }
}
