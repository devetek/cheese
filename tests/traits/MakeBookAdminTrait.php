<?php

use Faker\Factory as Faker;
use App\Models\BookAdmin;
use App\Repositories\Book\BookAdminRepository;

trait MakeBookAdminTrait
{
    /**
     * Create fake instance of BookAdmin and save it in database
     *
     * @param array $bookAdminFields
     * @return BookAdmin
     */
    public function makeBookAdmin($bookAdminFields = [])
    {
        /** @var BookAdminRepository $bookAdminRepo */
        $bookAdminRepo = App::make(BookAdminRepository::class);
        $theme = $this->fakeBookAdminData($bookAdminFields);
        return $bookAdminRepo->create($theme);
    }

    /**
     * Get fake instance of BookAdmin
     *
     * @param array $bookAdminFields
     * @return BookAdmin
     */
    public function fakeBookAdmin($bookAdminFields = [])
    {
        return new BookAdmin($this->fakeBookAdminData($bookAdminFields));
    }

    /**
     * Get fake data of BookAdmin
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookAdminData($bookAdminFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title_book' => $fake->word,
            'org_price' => $fake->randomDigitNotNull,
            'actual_price' => $fake->randomDigitNotNull,
            'description' => $fake->text,
            'promotional_text' => $fake->text,
            'id_publisher' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'cover_url' => $fake->word,
            'thum_cover_url' => $fake->word,
            'date_publish' => $fake->word,
            'status' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bookAdminFields);
    }
}
