<?php

use Faker\Factory as Faker;
use App\Models\packetAdmin;
use App\Repositories\Packet\packetAdminRepository;

trait MakepacketAdminTrait
{
    /**
     * Create fake instance of packetAdmin and save it in database
     *
     * @param array $packetAdminFields
     * @return packetAdmin
     */
    public function makepacketAdmin($packetAdminFields = [])
    {
        /** @var packetAdminRepository $packetAdminRepo */
        $packetAdminRepo = App::make(packetAdminRepository::class);
        $theme = $this->fakepacketAdminData($packetAdminFields);
        return $packetAdminRepo->create($theme);
    }

    /**
     * Get fake instance of packetAdmin
     *
     * @param array $packetAdminFields
     * @return packetAdmin
     */
    public function fakepacketAdmin($packetAdminFields = [])
    {
        return new packetAdmin($this->fakepacketAdminData($packetAdminFields));
    }

    /**
     * Get fake data of packetAdmin
     *
     * @param array $postFields
     * @return array
     */
    public function fakepacketAdminData($packetAdminFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name_packet' => $fake->word,
            'excerpt_packet' => $fake->text,
            'description_packet' => $fake->word,
            'id_publisher_team' => $fake->randomDigitNotNull,
            'cover_packet' => $fake->word,
            'thum_packet' => $fake->word,
            'date_publish' => $fake->word,
            'id_company' => $fake->randomDigitNotNull,
            'status_packet' => $fake->word,
            'is_public' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetAdminFields);
    }
}
