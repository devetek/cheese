<?php

use Faker\Factory as Faker;
use App\Models\Publisher_team;
use App\Repositories\Publisher\admin\Publisher_teamRepository;

trait MakePublisher_teamTrait
{
    /**
     * Create fake instance of Publisher_team and save it in database
     *
     * @param array $publisherTeamFields
     * @return Publisher_team
     */
    public function makePublisher_team($publisherTeamFields = [])
    {
        /** @var Publisher_teamRepository $publisherTeamRepo */
        $publisherTeamRepo = App::make(Publisher_teamRepository::class);
        $theme = $this->fakePublisher_teamData($publisherTeamFields);
        return $publisherTeamRepo->create($theme);
    }

    /**
     * Get fake instance of Publisher_team
     *
     * @param array $publisherTeamFields
     * @return Publisher_team
     */
    public function fakePublisher_team($publisherTeamFields = [])
    {
        return new Publisher_team($this->fakePublisher_teamData($publisherTeamFields));
    }

    /**
     * Get fake data of Publisher_team
     *
     * @param array $postFields
     * @return array
     */
    public function fakePublisher_teamData($publisherTeamFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_publisher' => $fake->randomDigitNotNull,
            'name_publisher_team' => $fake->word,
            'description_publisher_team' => $fake->word,
            'id_publisher_team_leader' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $publisherTeamFields);
    }
}
