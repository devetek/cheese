<?php

use Faker\Factory as Faker;
use App\Models\Packet_detail;
use App\Repositories\Packet\client\Packet_detailRepository;

trait MakePacket_detailTrait
{
    /**
     * Create fake instance of Packet_detail and save it in database
     *
     * @param array $packetDetailFields
     * @return Packet_detail
     */
    public function makePacket_detail($packetDetailFields = [])
    {
        /** @var Packet_detailRepository $packetDetailRepo */
        $packetDetailRepo = App::make(Packet_detailRepository::class);
        $theme = $this->fakePacket_detailData($packetDetailFields);
        return $packetDetailRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_detail
     *
     * @param array $packetDetailFields
     * @return Packet_detail
     */
    public function fakePacket_detail($packetDetailFields = [])
    {
        return new Packet_detail($this->fakePacket_detailData($packetDetailFields));
    }

    /**
     * Get fake data of Packet_detail
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_detailData($packetDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_packet' => $fake->randomDigitNotNull,
            'detail_packet' => $fake->word,
            'icon' => $fake->word,
            'order_detail' => $fake->randomDigitNotNull
        ], $packetDetailFields);
    }
}
