<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SantriApiTest extends TestCase
{
    use MakeSantriTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSantri()
    {
        $santri = $this->fakeSantriData();
        $this->json('POST', '/api/v1/santris', $santri);

        $this->assertApiResponse($santri);
    }

    /**
     * @test
     */
    public function testReadSantri()
    {
        $santri = $this->makeSantri();
        $this->json('GET', '/api/v1/santris/'.$santri->id_santri);

        $this->assertApiResponse($santri->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSantri()
    {
        $santri = $this->makeSantri();
        $editedSantri = $this->fakeSantriData();

        $this->json('PUT', '/api/v1/santris/'.$santri->id_santri, $editedSantri);

        $this->assertApiResponse($editedSantri);
    }

    /**
     * @test
     */
    public function testDeleteSantri()
    {
        $santri = $this->makeSantri();
        $this->json('DELETE', '/api/v1/santris/'.$santri->id_santri);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/santris/'.$santri->id_santri);

        $this->assertResponseStatus(404);
    }
}
