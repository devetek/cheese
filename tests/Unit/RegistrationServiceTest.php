<?php

use App\V1\Services\RegistrationService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory;
use Tests\TestCase;

class RegistrationServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected $test_registration_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();
        $this->test_registration_service = new RegistrationService;
        $this->faker = Factory::create();
    }

    public function testRegister()
    {
        $max = (int) env('MAX_VALUE_TEST');

        for ($i = 0; $i < $max; $i++) {
            $this->test_registration_service->register([
                'name' => $this->faker->name,
                'password' => bcrypt($this->faker->name),
                'email' => $this->faker->email,
            ]);
        }

        $this->assertTrue(true);
    }
}
