<?php

use App\Models\Ticket_answer;
use App\Repositories\Ticket\admin\Ticket_answerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Ticket_answerRepositoryTest extends TestCase
{
    use MakeTicket_answerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Ticket_answerRepository
     */
    protected $ticketAnswerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ticketAnswerRepo = App::make(Ticket_answerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTicket_answer()
    {
        $ticketAnswer = $this->fakeTicket_answerData();
        $createdTicket_answer = $this->ticketAnswerRepo->create($ticketAnswer);
        $createdTicket_answer = $createdTicket_answer->toArray();
        $this->assertArrayHasKey('id', $createdTicket_answer);
        $this->assertNotNull($createdTicket_answer['id'], 'Created Ticket_answer must have id specified');
        $this->assertNotNull(Ticket_answer::find($createdTicket_answer['id']), 'Ticket_answer with given id must be in DB');
        $this->assertModelData($ticketAnswer, $createdTicket_answer);
    }

    /**
     * @test read
     */
    public function testReadTicket_answer()
    {
        $ticketAnswer = $this->makeTicket_answer();
        $dbTicket_answer = $this->ticketAnswerRepo->find($ticketAnswer->id_ticket_answer);
        $dbTicket_answer = $dbTicket_answer->toArray();
        $this->assertModelData($ticketAnswer->toArray(), $dbTicket_answer);
    }

    /**
     * @test update
     */
    public function testUpdateTicket_answer()
    {
        $ticketAnswer = $this->makeTicket_answer();
        $fakeTicket_answer = $this->fakeTicket_answerData();
        $updatedTicket_answer = $this->ticketAnswerRepo->update($fakeTicket_answer, $ticketAnswer->id_ticket_answer);
        $this->assertModelData($fakeTicket_answer, $updatedTicket_answer->toArray());
        $dbTicket_answer = $this->ticketAnswerRepo->find($ticketAnswer->id_ticket_answer);
        $this->assertModelData($fakeTicket_answer, $dbTicket_answer->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTicket_answer()
    {
        $ticketAnswer = $this->makeTicket_answer();
        $resp = $this->ticketAnswerRepo->delete($ticketAnswer->id_ticket_answer);
        $this->assertTrue($resp);
        $this->assertNull(Ticket_answer::find($ticketAnswer->id_ticket_answer), 'Ticket_answer should not exist in DB');
    }
}
