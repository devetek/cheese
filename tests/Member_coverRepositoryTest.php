<?php

use App\Models\Member_cover;
use App\Repositories\Member\admin\Member_coverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Member_coverRepositoryTest extends TestCase
{
    use MakeMember_coverTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Member_coverRepository
     */
    protected $memberCoverRepo;

    public function setUp()
    {
        parent::setUp();
        $this->memberCoverRepo = App::make(Member_coverRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMember_cover()
    {
        $memberCover = $this->fakeMember_coverData();
        $createdMember_cover = $this->memberCoverRepo->create($memberCover);
        $createdMember_cover = $createdMember_cover->toArray();
        $this->assertArrayHasKey('id', $createdMember_cover);
        $this->assertNotNull($createdMember_cover['id'], 'Created Member_cover must have id specified');
        $this->assertNotNull(Member_cover::find($createdMember_cover['id']), 'Member_cover with given id must be in DB');
        $this->assertModelData($memberCover, $createdMember_cover);
    }

    /**
     * @test read
     */
    public function testReadMember_cover()
    {
        $memberCover = $this->makeMember_cover();
        $dbMember_cover = $this->memberCoverRepo->find($memberCover->id_member);
        $dbMember_cover = $dbMember_cover->toArray();
        $this->assertModelData($memberCover->toArray(), $dbMember_cover);
    }

    /**
     * @test update
     */
    public function testUpdateMember_cover()
    {
        $memberCover = $this->makeMember_cover();
        $fakeMember_cover = $this->fakeMember_coverData();
        $updatedMember_cover = $this->memberCoverRepo->update($fakeMember_cover, $memberCover->id_member);
        $this->assertModelData($fakeMember_cover, $updatedMember_cover->toArray());
        $dbMember_cover = $this->memberCoverRepo->find($memberCover->id_member);
        $this->assertModelData($fakeMember_cover, $dbMember_cover->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMember_cover()
    {
        $memberCover = $this->makeMember_cover();
        $resp = $this->memberCoverRepo->delete($memberCover->id_member);
        $this->assertTrue($resp);
        $this->assertNull(Member_cover::find($memberCover->id_member), 'Member_cover should not exist in DB');
    }
}
