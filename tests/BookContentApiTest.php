<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookContentApiTest extends TestCase
{
    use MakeBookContentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBookContent()
    {
        $bookContent = $this->fakeBookContentData();
        $this->json('POST', '/api/v1/bookContents', $bookContent);

        $this->assertApiResponse($bookContent);
    }

    /**
     * @test
     */
    public function testReadBookContent()
    {
        $bookContent = $this->makeBookContent();
        $this->json('GET', '/api/v1/bookContents/'.$bookContent->id_book_content);

        $this->assertApiResponse($bookContent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBookContent()
    {
        $bookContent = $this->makeBookContent();
        $editedBookContent = $this->fakeBookContentData();

        $this->json('PUT', '/api/v1/bookContents/'.$bookContent->id_book_content, $editedBookContent);

        $this->assertApiResponse($editedBookContent);
    }

    /**
     * @test
     */
    public function testDeleteBookContent()
    {
        $bookContent = $this->makeBookContent();
        $this->json('DELETE', '/api/v1/bookContents/'.$bookContent->id_book_content);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bookContents/'.$bookContent->id_book_content);

        $this->assertResponseStatus(404);
    }
}
