<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MyPacketApiTest extends TestCase
{
    use MakeMyPacketTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMyPacket()
    {
        $myPacket = $this->fakeMyPacketData();
        $this->json('POST', '/api/v1/myPackets', $myPacket);

        $this->assertApiResponse($myPacket);
    }

    /**
     * @test
     */
    public function testReadMyPacket()
    {
        $myPacket = $this->makeMyPacket();
        $this->json('GET', '/api/v1/myPackets/'.$myPacket->id_packet);

        $this->assertApiResponse($myPacket->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMyPacket()
    {
        $myPacket = $this->makeMyPacket();
        $editedMyPacket = $this->fakeMyPacketData();

        $this->json('PUT', '/api/v1/myPackets/'.$myPacket->id_packet, $editedMyPacket);

        $this->assertApiResponse($editedMyPacket);
    }

    /**
     * @test
     */
    public function testDeleteMyPacket()
    {
        $myPacket = $this->makeMyPacket();
        $this->json('DELETE', '/api/v1/myPackets/'.$myPacket->id_packet);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/myPackets/'.$myPacket->id_packet);

        $this->assertResponseStatus(404);
    }
}
