<?php

use App\Models\PacketIncome;
use App\Repositories\Packet\admin\PacketIncomeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PacketIncomeRepositoryTest extends TestCase
{
    use MakePacketIncomeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PacketIncomeRepository
     */
    protected $packetIncomeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetIncomeRepo = App::make(PacketIncomeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacketIncome()
    {
        $packetIncome = $this->fakePacketIncomeData();
        $createdPacketIncome = $this->packetIncomeRepo->create($packetIncome);
        $createdPacketIncome = $createdPacketIncome->toArray();
        $this->assertArrayHasKey('id', $createdPacketIncome);
        $this->assertNotNull($createdPacketIncome['id'], 'Created PacketIncome must have id specified');
        $this->assertNotNull(PacketIncome::find($createdPacketIncome['id']), 'PacketIncome with given id must be in DB');
        $this->assertModelData($packetIncome, $createdPacketIncome);
    }

    /**
     * @test read
     */
    public function testReadPacketIncome()
    {
        $packetIncome = $this->makePacketIncome();
        $dbPacketIncome = $this->packetIncomeRepo->find($packetIncome->id_packet);
        $dbPacketIncome = $dbPacketIncome->toArray();
        $this->assertModelData($packetIncome->toArray(), $dbPacketIncome);
    }

    /**
     * @test update
     */
    public function testUpdatePacketIncome()
    {
        $packetIncome = $this->makePacketIncome();
        $fakePacketIncome = $this->fakePacketIncomeData();
        $updatedPacketIncome = $this->packetIncomeRepo->update($fakePacketIncome, $packetIncome->id_packet);
        $this->assertModelData($fakePacketIncome, $updatedPacketIncome->toArray());
        $dbPacketIncome = $this->packetIncomeRepo->find($packetIncome->id_packet);
        $this->assertModelData($fakePacketIncome, $dbPacketIncome->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacketIncome()
    {
        $packetIncome = $this->makePacketIncome();
        $resp = $this->packetIncomeRepo->delete($packetIncome->id_packet);
        $this->assertTrue($resp);
        $this->assertNull(PacketIncome::find($packetIncome->id_packet), 'PacketIncome should not exist in DB');
    }
}
