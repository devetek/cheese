<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Purchase_packetApiTest extends TestCase
{
    use MakePurchase_packetTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePurchase_packet()
    {
        $purchasePacket = $this->fakePurchase_packetData();
        $this->json('POST', '/api/v1/purchasePackets', $purchasePacket);

        $this->assertApiResponse($purchasePacket);
    }

    /**
     * @test
     */
    public function testReadPurchase_packet()
    {
        $purchasePacket = $this->makePurchase_packet();
        $this->json('GET', '/api/v1/purchasePackets/'.$purchasePacket->id_purchase);

        $this->assertApiResponse($purchasePacket->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePurchase_packet()
    {
        $purchasePacket = $this->makePurchase_packet();
        $editedPurchase_packet = $this->fakePurchase_packetData();

        $this->json('PUT', '/api/v1/purchasePackets/'.$purchasePacket->id_purchase, $editedPurchase_packet);

        $this->assertApiResponse($editedPurchase_packet);
    }

    /**
     * @test
     */
    public function testDeletePurchase_packet()
    {
        $purchasePacket = $this->makePurchase_packet();
        $this->json('DELETE', '/api/v1/purchasePackets/'.$purchasePacket->id_purchase);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/purchasePackets/'.$purchasePacket->id_purchase);

        $this->assertResponseStatus(404);
    }
}
