generate-docs:
	@ php tools/swagger-generator.php

run-dev:
	@ test -f docker/mysql/volume || mkdir -p docker/mysql/volume
	@ test -d vendor || composer update -vvv
	@ test -d vendor || composer install -vvv
	@ docker-compose -f docker/dev.docker-compose.yaml up